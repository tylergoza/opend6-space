// Import Modules
import {OD6SActor} from "./actor/actor.js";
import {OD6SActorSheet} from "./actor/actor-sheet.js";
import {OD6SItem} from "./item/item.js";
import {OD6SItemSheet} from "./item/item-sheet.js";
import {OD6SToken} from "./overrides/token.js";
import {od6sutilities} from "./system/utilities.js";
import {OD6SCombatTracker} from "./overrides/combat-tracker.js";
import {OD6SCompendiumDirectory} from "./overrides/compendium-directory.js";
import {OD6SChatLog} from "./overrides/chat-log.js";
import OD6SEditDifficulty, {OD6SEditDamage, OD6SChooseTarget, OD6SChat, OD6SHandleWildDieForm} from "./apps/chat.js";
import OD6SSocketHandler from "./system/socket.js";
import OD6S from "./config/config-od6s.js";
import od6sSettings from "./config/settings-od6s.js";
import od6sHandlebars from "./system/handlebars.js"

od6sSettings();
od6sHandlebars();

Hooks.once('init', async function () {

    game.od6s = {
        OD6SActor,
        OD6SItem,
        OD6SToken,
        rollItemMacro,
        rollItemNameMacro,
        simpleRoll,
        getActorFromUuid,
        diceTerms: [CharacterPointDie, WildDie],
        config: OD6S
    };

    //CONFIG.debug.hooks = true

    game.socket.on('system.od6s', (data) => {
        if (data.operation === 'updateRollMessage') OD6SSocketHandler.updateRollMessage(data);
        if (data.operation === 'updateInitRoll') OD6SSocketHandler.updateInitRoll(data);
        if (data.operation === 'addToVehicle') OD6SSocketHandler.addToVehicle(data);
        if (data.operation === 'removeFromVehicle') OD6SSocketHandler.removeFromVehicle(data);
        if (data.operation === 'sendVehicleStats') OD6SSocketHandler.sendVehicleStats(data);
    })

    /**
     * Set an initiative formula for the system
     * @type {String}
     */
    CONFIG.Combat.initiative = {
        formula: "@initiative.formula",
        decimals: 2
    };

    if (typeof Babele !== 'undefined') {
        Babele.get().setSystemTranslationsDir("lang/translations");
    }

    CONFIG.ui.chat = OD6SChatLog;
    CONFIG.ui.combat = OD6SCombatTracker;
    CONFIG.ui.compendium = OD6SCompendiumDirectory;
    CONFIG.statusEffects = OD6S.statusEffects;
    CONFIG.Dice.terms["w"] = WildDie;
    CONFIG.Dice.terms["b"] = CharacterPointDie;
    CONFIG.Token.objectClass = OD6SToken;

    CONFIG.ChatMessage.template = "systems/od6s/templates/chat/chat.html";

    // Define custom Entity classes
    CONFIG.Actor.documentClass = OD6SActor;
    CONFIG.Item.documentClass = OD6SItem;

    // Register sheet application classes
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("od6s", OD6SActorSheet, {makeDefault: true});
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("od6s", OD6SItemSheet, {makeDefault: true});
});

// Chat listeners
Hooks.on('renderChatMessage', (msg, html, data) => {
    if (game.settings.get('od6s', 'hide-gm-rolls') && data.whisperTo !== '') {
        if (game.user.isGM === false &&
            game.user.data.id !== data.author.id &&
            data.message.whisper.indexOf(game.user.id) === -1) {
            msg.data.sound = null;
            html.hide();
        }
    }
})

Hooks.on("updateChatMessage", (message, data, diff, id) => {
    if (data.blind === false) {
        let messageLi = $(`.message[data-message-id=${data._id}]`);
        messageLi.show();
    }
});

Hooks.on('renderChatLog', (log, html, data) => {
    html.on("click", ".modifiers-button", async ev => {
        let content = document.getElementById("modifiers-display-" + ev.currentTarget.dataset.messageId);
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
        game.messages.get(ev.currentTarget.dataset.messageId).render();
    })

    html.on("click", ".damage-modifiers-button", async ev => {
        let content = document.getElementById("damage-modifiers-display-" + ev.currentTarget.dataset.messageId);
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
        game.messages.get(ev.currentTarget.dataset.messageId).render();
    })

    html.on("click", ".apply-damage-button", async ev => {
        ev.preventDefault();
        let actor = await od6sutilities.getActorFromUuid(game.scenes.active.tokens.get(ev.currentTarget.dataset.tokenId).uuid);
        const result = ev.currentTarget.dataset.result;
        const isVehicle = ev.currentTarget.dataset.isVehicle;
        const messageId = ev.currentTarget.dataset.messageId;
        const update = {};

        const msg = game.messages.get(messageId);

        if (isVehicle === true || isVehicle === 'true') {
            actor = await od6sutilities.getActorFromUuid(actor.data.data.vehicle.uuid);
        }

        update.id = actor.id;
        if (game.settings.get('od6s', 'bodypoints') === 0 || (isVehicle === true || isVehicle === 'true')
            || actor.type === 'starship' || actor.type === 'vehicle') {
            if (isVehicle === true || isVehicle === 'true') {
                actor.applyDamage(result);
            } else {
                actor.applyWounds(result);
            }
        } else {
            let bp = actor.data.data.wounds.body_points.current - result;
            if (bp < 0) bp = 0;
            update['data.wounds.body_points.current'] = bp;
        }
        actor.update(update);
        msg.setFlag('od6s', 'applied', true);
    })

    html.on("click", ".damage-button", async ev => {
        ev.preventDefault();
        const data = ev.currentTarget.dataset;
        const dice = {};
        dice.dice = data.damageDice;
        dice.pips = data.damagePips;
        let rollString;

        if (game.settings.get('od6s', 'use_wild_die')) {
            dice.dice = dice.dice - 1;
            if (dice.dice < 1) {
                rollString = "+1dw" + game.i18n.localize("OD6S.WILD_DIE_FLAVOR");
            } else {
                rollString = dice.dice + "d6" + game.i18n.localize('OD6S.BASE_DIE_FLAVOR') + "+1dw" +
                    game.i18n.localize("OD6S.WILD_DIE_FLAVOR");
            }
        } else {
            rollString = dice.dice + "d6" + game.i18n.localize('OD6S.BASE_DIE_FLAVOR');
        }
        dice.pips ? rollString += "+" + dice.pips : null;
        if (!game.settings.get('od6s', 'dice_for_scale')) {
            if (data.damagescalebonus > 0) rollString += "+" + Math.abs(data.damagescalebonus);
            if (data.damagescalebonus < 0) rollString += "-" + Math.abs(data.damagescalebonus);
        }

        let roll = await new Roll(rollString).evaluate({"async": true});

        let label = game.i18n.localize('OD6S.DAMAGE') + " (" +
            game.i18n.localize(OD6S.damageTypes[data.damagetype]) + ")";

        if (typeof (data.source) !== 'undefined' && data.source !== '') {
            label = label + " " + game.i18n.localize('OD6S.FROM') + " " +
                game.i18n.localize(data.source);
        }

        if (typeof (data.vehicle) !== 'undefined' && data.vehicle !== '') {
            const vehicle = await od6sutilities.getActorFromUuid(data.vehicle);
            label = label + " " + game.i18n.localize('OD6S.BY') + " " + vehicle.name;
        }

        if (typeof (data.targetname) !== 'undefined' && data.targetname !== '') {
            label = label + " " + game.i18n.localize('OD6S.TO') + " " + data.targetname;
        }

        data.collision = (data.collision === 'true');

        const flags = {
            "type": "damage",
            "source": data.source,
            "damageType": data.damagetype,
            "targetName": data.targetname,
            "targetId": data.targetid,
            "attackMessage": data.messageId,
            "isOpposable": true,
            "wild": false,
            "wildHandled": false,
            "wildResult": OD6S.wildDieResult[OD6S.wildDieOneDefault],
            "total": roll.total,
            "isVehicleCollision": data.collision
        }

        if (game.settings.get('od6s', 'use_wild_die')) {
            if (roll.terms.find(d => d.flavor === "Wild").total === 1) {
                flags.wild = true;
                if (OD6S.wildDieOneDefault > 0 && OD6S.wildDieOneAuto === 0) {
                    flags.wildHandled = true;
                }
            } else {
                flags.wild = false;
            }
        }

        let rollMode = 'roll';
        if (game.user.isGM && game.settings.get('od6s', 'hide-gm-rolls')) rollMode = CONST.DICE_ROLL_MODES.PRIVATE;

        let rollMessage = await roll.toMessage({
            speaker: ChatMessage.getSpeaker({actor: game.actors.find(a => a.id === data.actor)}),
            flavor: label,
            flags: {
                od6s: flags
            },
            rollMode: rollMode, create: true
        });

        if (flags.wild === true && OD6S.wildDieOneDefault === 2 && OD6S.wildDieOneAuto === 0) {
            let replacementRoll = JSON.parse(JSON.stringify(rollMessage.roll.toJSON()));
            let highest = 0;
            for (let i = 0; i < replacementRoll.terms[0].results.length; i++) {
                replacementRoll.terms[0].results[i].result >
                replacementRoll.terms[0].results[highest].result ?
                    highest = i : {}
            }
            replacementRoll.terms[0].results[highest].discarded = true;
            replacementRoll.terms[0].results[highest].active = false;
            replacementRoll.total -= (+replacementRoll.terms[0].results[highest].result);
            const rollMessageUpdate = {};
            rollMessageUpdate.data = {};
            rollMessageUpdate.content = replacementRoll.total;
            rollMessageUpdate.id = rollMessage.id;
            rollMessageUpdate.roll = replacementRoll;

            if (game.user.isGM) {
                if (rollMessage.getFlag('od6s', 'difficulty') && rollMessage.getFlag('od6s', 'success')) {
                    replacementRoll.total < rollMessage.getFlag('od6s', 'difficulty') ? await rollMessage.setFlag('od6s', 'success', false) :
                        await rollMessage.setFlag('od6s', 'success', true);
                }
                await rollMessage.setFlag('od6s', 'originalroll', rollMessage.roll)
                await rollMessage.update(rollMessageUpdate, {"diff": true});
            } else {
                game.socket.emit('system.od6s', {
                    operation: 'updateRollMessage',
                    message: rollMessage,
                    update: rollMessageUpdate
                })
            }
        }
    })

    html.on("click", ".edit-difficulty", async ev => {
        ev.preventDefault();
        let data = {};
        const dataSet = ev.currentTarget.dataset;
        data.messageId = dataSet.messageId;
        const message = game.messages.get(dataSet.messageId);
        data.baseDifficulty = message.getFlag('od6s', 'baseDifficulty');
        data.modifiers = message.getFlag('od6s', 'modifiers');
        new OD6SEditDifficulty(data).render(true);
    })

    html.on("click", ".edit-damage", async ev => {
        ev.preventDefault();
        let data = {};
        const dataSet = ev.currentTarget.dataset;
        data.messageId = dataSet.messageId;
        const message = game.messages.get(dataSet.messageId);
        data.damage = message.getFlag('od6s', 'damageScore');
        data.damageDice = message.getFlag('od6s', 'damageDice');
        new OD6SEditDamage(data).render(true);
    })

    html.on("click", ".choose-target", async ev => {
        ev.preventDefault();
        data = {};
        data.targets = [];
        data.messageId = ev.currentTarget.dataset.messageId;
        if (game.user.isGM) {
            // If in combat, only load tokens in combat.  Otherwise, load all tokens in scene
            if (game.combat) {
                for (let t of game.combat.combatants) {
                    const target = {
                        "id": t.token.id,
                        "name": t.token.name
                    }
                    data.targets.push(target);

                }
            } else {
                data.targets = game.scenes.active.data.tokens;
            }
        } else {
            return;
        }
        new OD6SChooseTarget(data).render(true);
    })

    html.on("click", ".wilddiegm", async ev => {
        ev.preventDefault();
        // three choices: leave it as-is, remove the highest die from the roll, or cause a complication
        new OD6SHandleWildDieForm(ev).render(true);
    })

    html.on("click", ".message-reveal", async ev => {
        const message = game.messages.get(ev.currentTarget.dataset.messageId);
        await message.setFlag('od6s', 'isVisible', true);
        await message.setFlag('od6s', 'isKnown', true);
    })

    html.on("click", ".message-oppose", async ev => {
        ev.preventDefault();
        // Check if there are any opposed cards already in the pipe
        if (OD6S.opposed.length > 0) {
            OD6S.opposed.push(ev.currentTarget.dataset.messageId);
            return od6sutilities.handleOpposedRoll();
        } else {
            OD6S.opposed.push(ev.currentTarget.dataset.messageId);
        }
    })
})

// Custom dice for DiceSoNice
Hooks.on('diceSoNiceReady', (dice3d) => {
    dice3d.addSystem({id: 'od6s', name: "OpenD6 Space"}, "default")
    dice3d.addDicePreset({
        type: "dw",
        labels: [game.settings.get('od6s', 'wild_die_one_face'), "2", "3", "4", "5", game.settings.get('od6s', 'wild_die_six_face')],
        colorset: "white",
        values: {min: 1, max: 6},
        system: "od6s"
    }, "dw")
    dice3d.addDicePreset({
        type: "db",
        labels: ["1", "2", "3", "4", "5", "6"],
        colorset: "black",
        values: {min: 1, max: 6},
        system: "od6s"
    }, "db")
})

Hooks.on('diceSoNiceRollStart', (messageId, context) => {
    const roll = context.roll;
    let die;
    let len = roll.dice.length;
    // Customize colors for Dice So Nice
    for (die = 0; die < len; die++) {
        switch (roll.dice[die].options.flavor) {
            case "Wild":
                roll.dice[die].options.colorset = "white";
                break;
            case "CP":
                roll.dice[die].options.colorset = "bronze";
                break;
            case "Bonus":
                roll.dice[die].options.colorset = "black";
                break;
            default:
                break;
        }
    }
})

Hooks.on('renderChatMessage', (message, html, data) => {
    ui.chat.scrollBottom();
})

Hooks.on('deleteActiveEffect', async (effect) => {
    for (let change of effect.data.changes) {
        if (change.key.startsWith("data.items")) {
            const t = change.key.split('.');
            if (t[2] === 'skills' || t[2] === 'skill' || t[2] === 'specializations' || t[2] === 'specialization') {
                const item = effect.parent.items.find(i => i.name === t[3]);
                if (typeof (item) !== 'undefined') {
                    const update = {};
                    update.id = item.id;
                    update.data = {};
                    update.data[t[4]] = {};
                    if (change.mode === 2) {
                        update.data[t[4]] -= (+change.value);
                        item.update(update);
                    }
                }
            } else if (t[2] === 'weapon' || t[2] === 'weapons') {
                const item = effect.parent.items.find(i => i.name === t[3]);
                if (typeof (item) !== 'undefined') {
                    const update = {};
                    update.id = item.id;
                    update.data = {};
                    if (t[4] === 'mods') {
                        update.data[t[4]] = {};
                        if (change.mode === 2) {
                            update.data[t[4]][t[5]] -= (+change.value);
                            item.update(update);
                        }
                    } else {
                        if (change.mode === 2) {
                            update.data[t[4]] -= (+change);
                            item.update(update);
                        }
                    }
                }
            }
        }
    }
})

Hooks.on("canvasReady", async () => {
    //Loop through all vehicle actors/tokens and sync vehicle data
    if (game.user.isGM) {
        game.scenes.active.data.tokens.forEach((values, keys) => {
            if (values.actor.type === "vehicle" || values.actor.type === "starship") {
                values.actor.sendVehicleData();
            }
        })
        game.actors.forEach((values, keys) => {
            if (values.type === "vehicle" || values.type === "starship") {
                values.sendVehicleData();
            }
        })
    }
})

Hooks.on("getOD6SChatLogEntryContext", async (html, options) => {
    await OD6SChat.chatContextMenu(html, options);
})

Hooks.on("updateActor", async (document, change, options, userId) => {
    if ((document.type === "vehicle" || document.type === "starship") && document.data.data.crewmembers.length > 0) {
        await document.sendVehicleData();
    }
})

Hooks.on("updateToken", async (document, change, options, userId) => {
    if ((document.type === "vehicle" || document.type === "starship") && document.data.data.crewmembers.length > 0) {
        await document.sendVehicleData();
    }
})

Hooks.on("renderActorSheet", async (sheet) => {
    if ((sheet.actor.type === "vehicle" || sheet.actor.type === "starship") && sheet.actor.data.data.crewmembers.length > 0) {
        await sheet.actor.sendVehicleData();
    }
})

Hooks.once("ready", async function () {
    // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
    Hooks.on("hotbarDrop", (bar, data, slot) => createOD6SMacro(data, slot));

    // Set customizations
    OD6S.fatePointsName = game.settings.get('od6s', 'customize_fate_points') ?
        game.settings.get('od6s', 'customize_fate_points') : game.i18n.localize('OD6S.CHAR_FATE_POINTS');
    OD6S.fatePointsShortName = game.settings.get('od6s', 'customize_fate_points_short') ?
        game.settings.get('od6s', 'customize_fate_points_short') : game.i18n.localize('OD6S.CHAR_FATE_POINTS_SHORT');

    OD6S.manifestationsName = game.settings.get('od6s', 'customize_manifestations') ?
        game.settings.get('od6s', 'customize_manifestations') : game.i18n.localize('OD6S.CHAR_MANIFESTATIONS');

    OD6S.manifestationName = game.settings.get('od6s', 'customize_manifestation') ?
        game.settings.get('od6s', 'customize_manifestation') : game.i18n.localize('OD6S.CHAR_MANIFESTATION');

    OD6S.metaphysicsExtranormalName = game.settings.get('od6s', 'customize_metaphysics_extranormal') ?
        game.settings.get('od6s', 'customize_metaphysics_extranormal') : game.i18n.localize('OD6S.CHAR_METAPHYSICS_EXTRANORMAL');

    OD6S.vehicleToughnessName = game.settings.get('od6s', 'customize_vehicle_toughness') ?
        game.settings.get('od6s', 'customize_vehicle_toughness') : game.i18n.localize('OD6S.TOUGHNESS');

    OD6S.starshipToughnessName = game.settings.get('od6s', 'customize_starship_toughness') ?
        game.settings.get('od6s', 'customize_starship_toughness') : game.i18n.localize('OD6S.TOUGHNESS');

    OD6S.useAFatePointName = game.settings.get('od6s', 'customize_use_a_fate_point') ?
        game.settings.get('od6s', 'customize_use_a_fate_point') : game.i18n.localize('OD6S.USE_FATE_POINT');

    OD6S.attributes.agi.name = game.settings.get('od6s', 'customize_agility_name') ?
        game.settings.get('od6s', 'customize_agility_name') : game.i18n.localize('OD6S.CHAR_AGILITY');
    OD6S.attributes.agi.shortName = game.settings.get('od6s', 'customize_agility_name_short') ?
        game.settings.get('od6s', 'customize_agility_name_short') : game.i18n.localize('OD6S.CHAR_AGILITY_SHORT');

    OD6S.attributes.str.name = game.settings.get('od6s', 'customize_strength_name') ?
        game.settings.get('od6s', 'customize_strength_name') : game.i18n.localize('OD6S.CHAR_STRENGTH');
    OD6S.attributes.str.shortName = game.settings.get('od6s', 'customize_strength_name_short') ?
        game.settings.get('od6s', 'customize_strength_name_short') : game.i18n.localize('OD6S.CHAR_STRENGTH_SHORT');

    OD6S.attributes.mec.name = game.settings.get('od6s', 'customize_mechanical_name') ?
        game.settings.get('od6s', 'customize_mechanical_name') : game.i18n.localize('OD6S.CHAR_MECHANICAL');
    OD6S.attributes.mec.shortName = game.settings.get('od6s', 'customize_mechanical_name_short') ?
        game.settings.get('od6s', 'customize_mechanical_name_short') : game.i18n.localize('OD6S.CHAR_MECHANICAL_SHORT');

    OD6S.attributes.kno.name = game.settings.get('od6s', 'customize_knowledge_name') ?
        game.settings.get('od6s', 'customize_knowledge_name') : game.i18n.localize('OD6S.CHAR_KNOWLEDGE');
    OD6S.attributes.kno.shortName = game.settings.get('od6s', 'customize_knowledge_name_short') ?
        game.settings.get('od6s', 'customize_knowledge_name_short') : game.i18n.localize('OD6S.CHAR_KNOWLEDGE_SHORT');

    OD6S.attributes.per.name = game.settings.get('od6s', 'customize_perception_name') ?
        game.settings.get('od6s', 'customize_perception_name') : game.i18n.localize('OD6S.CHAR_PERCEPTION');
    OD6S.attributes.per.shortName = game.settings.get('od6s', 'customize_perception_name_short') ?
        game.settings.get('od6s', 'customize_perception_name_short') : game.i18n.localize('OD6S.CHAR_PERCEPTION_SHORT');

    OD6S.attributes.tec.name = game.settings.get('od6s', 'customize_technical_name') ?
        game.settings.get('od6s', 'customize_technical_name') : game.i18n.localize('OD6S.CHAR_TECHNICAL');
    OD6S.attributes.tec.shortName = game.settings.get('od6s', 'customize_technical_name_short') ?
        game.settings.get('od6s', 'customize_technical_name_short') : game.i18n.localize('OD6S.CHAR_TECHNICAL_SHORT');

    OD6S.attributes.met.name = game.settings.get('od6s', 'customize_metaphysics_name') ?
        game.settings.get('od6s', 'customize_metaphysics_name') : game.i18n.localize('OD6S.CHAR_METAPHYSICS');
    OD6S.attributes.met.shortName = game.settings.get('od6s', 'customize_metaphysics_name_short') ?
        game.settings.get('od6s', 'customize_metaphysics_name_short') : game.i18n.localize('OD6S.CHAR_METAPHYSICS_SHORT');

    OD6S.interstellarDriveName = game.settings.get('od6s', 'interstellar_drive_name') ?
        game.settings.get('od6s', 'interstellar_drive_name') : game.i18n.localize('OD6S.INTERSTELLAR_DRIVE');

    OD6S.bodyPointsName = game.settings.get('od6s', 'customize_body_points_name') ?
        game.settings.get('od6s', 'customize_body_points_name') : game.i18n.localize('OD6S.BODY_POINTS');

    game.i18n.translations.ITEM.TypeManifestation = OD6S.manifestationName;

    OD6S.wildDieOneDefault = game.settings.get('od6s', 'default_wild_one');
    OD6S.wildDieOneAuto = game.settings.get('od6s', 'default_wild_die_one_handle');

    OD6S.vehicleDifficulty = game.settings.get('od6s', 'vehicle_difficulty');

    OD6S.passengerDamageDice = game.settings.get('od6s', 'passenger_damage_dice');

    OD6S.grenadeDamageDice = game.settings.get('od6s', 'dice_for_grenades');

    OD6S.highlightEffects = game.settings.get('od6s', 'highlight_effects');

    OD6S.randomHitLocations = game.settings.get('od6s', 'random_hit_locations');

    OD6S.mapRange = game.settings.get('od6s', 'map_range_to_difficulty');
    OD6S.meleeDifficulty = game.settings.get('od6s', 'melee_difficulty');

    OD6S.baseAttackDifficulty = game.settings.get('od6s', 'default_attack_difficulty');

    OD6S.currencyName = game.settings.get('od6s', 'customize_currency_label') ?
        game.settings.get('od6s', 'customize_currency_label') : game.i18n.localize('OD6S.CHAR_CREDITS');

    if (game.settings.get('od6s', 'default_difficulty_very_easy'))
        OD6S.difficulty["OD6S.DIFFICULTY_VERY_EASY"].max = game.settings.get('od6s', 'default_difficulty_very_easy')

    if (game.settings.get('od6s', 'default_difficulty_easy'))
        OD6S.difficulty["OD6S.DIFFICULTY_EASY"].max = game.settings.get('od6s', 'default_difficulty_easy');

    if (game.settings.get('od6s', 'default_difficulty_moderate'))
        OD6S.difficulty["OD6S.DIFFICULTY_MODERATE"].max = game.settings.get('od6s', 'default_difficulty_moderate');

    if (game.settings.get('od6s', 'default_difficulty_difficult'))
        OD6S.difficulty["OD6S.DIFFICULTY_DIFFICULT"].max = game.settings.get('od6s', 'default_difficulty_difficult');

    if (game.settings.get('od6s', 'default_difficulty_very_difficult'))
        OD6S.difficulty["OD6S.DIFFICULTY_VERY_DIFFICULT"].max = game.settings.get('od6s', 'default_difficulty_very_difficult');

    if (game.settings.get('od6s', 'default_difficulty_heroic'))
        OD6S.difficulty["OD6S.DIFFICULTY_HEROIC"].max = game.settings.get('od6s', 'default_difficulty_heroic');

    if (game.settings.get('od6s', 'default_difficulty_legendary'))
        OD6S.difficulty["OD6S.DIFFICULTY_LEGENDARY"].max = game.settings.get('od6s', 'default_difficulty_legendary');

    if (game.settings.get('od6s', 'parry_skills')) {
        OD6S.actions.parry.skill = "OD6S.MELEE_PARRY";
        OD6S.actions.block.skill = "OD6S.BRAWLING_PARRY";
        OD6S.actions.block.name = "OD6S.BRAWLING_PARRY";
    } else {
        OD6S.actions.parry.skill = "OD6S.MELEE_COMBAT";
        OD6S.actions.block.skill = "OD6S.BRAWL";
        OD6S.actions.block.name = "OD6S.ACTION_BRAWL_BLOCK"
    }

    OD6S.fatePointRound = game.settings.get('od6s', 'fate_point_round');
    OD6S.fatePointClimactic = game.settings.get('od6s', 'fate_point_climactic');

    OD6S.woundConfig = game.settings.get('od6s', 'bodypoints');

    OD6S.highHitDamage = game.settings.get('od6s', 'highhitdamage');

    OD6S.autoOpposed = game.settings.get('od6s', 'auto_opposed')

    OD6S.cost = game.settings.get('od6s','cost');
    OD6S.fundsFate = game.settings.get('od6s', 'funds_fate');

    OD6S.opposed = [];

    OD6S.pipsPerDice = game.settings.get('od6s', 'pip_per_dice');

    OD6S.deadlinessLevel['character'] = game.settings.get('od6s', 'deadliness');
    OD6S.deadlinessLevel['npc'] = game.settings.get('od6s', 'npc-deadliness');
    OD6S.deadlinessLevel['creature'] = game.settings.get('od6s', 'creature-deadliness');

    OD6S.flatSkills = game.settings.get('od6s','flat_skills');

    OD6S.showSkillSpecialization = game.settings.get('od6s', 'show_skill_specialization');

    if (game.settings.get('od6s', 'customize_metaphysics_skill_channel'))
        OD6S.channelSkillName = game.settings.get('od6s', 'customize_metaphysics_skill_channel');
    if (game.settings.get('od6s', 'customize_metaphysics_skill_sense'))
        OD6S.senseSkillName = game.settings.get('od6s', 'customize_metaphysics_skill_sense');
    if (game.settings.get('od6s', 'customize_metaphysics_skill_transform'))
        OD6S.transformSkillName = game.settings.get('od6s', 'customize_metaphysics_skill_transform');

});

Hooks.on("updateCombat", async (Combat, data) => {
    if (game.user.isGM && Combat.data.round === 1 && Combat.data.turn === 0 && Combat.data.active && OD6S.startCombat) {
        // At the start of a new combat, make sure all actor's action lists are cleared
        OD6S.startCombat = false;
        for (let i = 0; i < Combat.combatants.length; i++) {
            await clearActionList(Combat.combatants[i].actor);
        }
    }

    if (game.user.isGM && Combat.data.round !== 0 && Combat.data.turn === 0 && Combat.data.active && !OD6S.startCombat) {
        // New round
    }

    // Actor has started their turn, clear their action list and defensive bonuses/penalties
    if (typeof (Combat.combatant.actor) !== 'undefined') {
        if (!game.settings.get('od6s', 'reaction_skills')) {
            const update = {};
            update.id = Combat.combatant.actor.id;
            update.data = {};
            update.data.parry = {};
            update.data.parry.score = 0;
            update.data.dodge = {};
            update.data.dodge.score = 0;
            update.data.block = {};
            update.data.block.score = 0;
            await Combat.combatant.actor.update(update, {'diff': true});
        }
        if (!OD6S.fatePointRound) {
            await Combat.combatant.actor.setFlag('od6s', 'fatepointeffect', false);
        }
    }
})

Hooks.on("preUpdateCombat", async (Combat, data) => {
    // End-of-turn stuff here
    if (data.turn === 0) {
        for (let i = 0; i < Combat.combatants.size; i++) {
            const combatant = Combat.combatants.contents[i].actor;
            if (typeof (combatant) !== 'undefined') {
                await clearActionList(combatant);
                if (game.settings.get('od6s', 'reaction_skills')) {
                    const update = {};
                    update.id = combatant.id;
                    update.data = {};
                    update.data.parry = {};
                    update.data.parry.score = 0;
                    update.data.dodge = {};
                    update.data.dodge.score = 0;
                    update.data.block = {};
                    update.data.block.score = 0;
                    await combatant.update(update, {'diff': true});
                }
                if (!OD6S.fatePointRound) {
                    await Combat.combatant.actor.setFlag('od6s', 'fatepointeffect', false);
                }
            }
        }
    }
})

Hooks.on("createCombat", async (Combat, combatant, info, data) => {
    if (!game.user.isGM) {
        return;
    }
    OD6S.startCombat = true;
})

Hooks.on("deleteCombat", async function (Combat) {
    // Combat is over, clear all combatant action lists
    for (let i = 0; i < Combat.combatants.length; i++) {
        await clearActionList(Combat.combatants[i].actor);
    }
})

Hooks.on('createChatMessage', async function (msg) {
    if (game.user.isGM) {
        if (msg.getFlag('od6s', 'isOpposable') && OD6S.autoOpposed
            && (msg.getFlag('od6s', 'type') === 'damage') || msg.getFlag('od6s', 'type') === 'resistance') {
            await od6sutilities.waitFor3DDiceMessage(msg.id);
            await od6sutilities.autoOpposeRoll(msg);
        }
    }
})

/**
 * Clear an actor's action list
 * @param actor
 * @returns {Promise<void>}
 */
async function clearActionList(actor) {
    if (actor !== null) {
        const actions = actor.itemTypes.action;
        for (let i = 0; i < actions.length; i++) {
            await actor.deleteEmbeddedDocuments('Item', [actions[i].id]);
        }
    }
}

/* -------------------------------------------- */
/*  Hotbar Macros                               */

/* -------------------------------------------- */

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {Object} data     The dropped data
 * @param {number} slot     The hotbar slot to use
 * @returns {Promise}
 */
async function createOD6SMacro(data, slot) {
    if (data.type !== "Item") return;
    if (!("data" in data)) return ui.notifications.warn(game.i18n.localize('OD6S.WARN_NOT_OWNED'));
    const item = data.data;

    // Filter out certain item types
    if (item.type === '' ||
        item.type === 'charactertemplate' ||
        item.type === 'action' ||
        item.type === 'disadvantage' ||
        item.type === 'advantage' ||
        item.type === 'armor' ||
        item.type === 'gear' ||
        item.type === 'cybernetic' ||
        item.type === 'vehicle') {
        return ui.notifications.warn(game.i18n.localize('OD6S.WARN_INVALID_MACRO_ITEM'));
    }

    // Create the macro command
    const command = `game.od6s.rollItemMacro("${item._id}");`;
    let macro = game.macros.find(m => (m.name === item.name) && (m.command === command));
    if (!macro) {
        macro = await Macro.create({
            name: item.name,
            type: "script",
            img: item.img,
            command: command,
            flags: {"od6s.itemMacro": true}
        });
    }
    await game.user.assignHotbarMacro(macro, slot);
    return false;
}

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {string} itemId
 * @return {Promise}
 */
export function rollItemMacro(itemId) {
    const speaker = ChatMessage.getSpeaker();
    let actor;
    if (speaker.token) actor = game.actors.tokens[speaker.token];
    if (!actor) actor = game.actors.get(speaker.actor);
    const item = actor ? actor.items.find(i => i.id === itemId) : null;
    if (!item) return ui.notifications.warn(game.i18n.localize('OD6S.WARN_NO_ITEM_ID') + " " + itemId);

    // Trigger the item roll
    return item.roll();
}

/**
 * Roll a Macro from an Item name.
 * @param {string} itemId
 * @return {Promise}
 */
export function rollItemNameMacro(name) {
    name = game.i18n.localize(name);
    const speaker = ChatMessage.getSpeaker();
    let actor;
    if (speaker.token) actor = game.actors.tokens[speaker.token];
    if (!actor) actor = game.actors.get(speaker.actor);
    const item = actor ? actor.items.find(i => i.name === name) : null;
    if (!item) return ui.notifications.warn(game.i18n.localize('OD6S.WARN_NO_ITEM_NAME') + " " + name);

    // Trigger the item roll
    return item.roll();
}

/**
 * Return either the customized or translated name of an attribute
 * @param attribute
 * @returns {string}
 */
export function getAttributeName(attribute) {
    attribute = attribute.toLowerCase();
    return OD6S.attributes[attribute].name;
}

/**
 * Return either the customized or translated short name of an attribute
 * @param attribute
 * @returns {string}
 */
export function getAttributeShortName(attribute) {
    attribute = attribute.toLowerCase();
    return OD6S.attributes[attribute].shortName;
}

async function simpleRoll() {
    const html = await renderTemplate("systems/od6s/templates/simpleRoll.html",
        {"wilddie": true, "dice": 1, "pips": 0});
    new Dialog({
        title: game.i18n.localize('OD6S.ROLL'),
        content: html,
        buttons: {
            roll: {
                label: game.i18n.localize('OD6S.ROLL'),
                callback: async (dlg) => {
                    let wild = false;
                    let rollString = "";
                    let rollMode = 0;
                    let dice = $(dlg[0]).find("#dice")[0].value;
                    let pips = $(dlg[0]).find("#pips")[0].value;
                    let damageRoll = $(dlg[0]).find('#damageroll')[0].checked;
                    let damageType = $(dlg[0]).find('#damagetype')[0].value;
                    if (game.settings.get('od6s', 'use_wild_die')) {
                        wild = $(dlg[0]).find("#wilddie")[0].checked;
                    } else {
                        wild = false;
                    }
                    if (wild) {
                        dice -= 1;
                        if (dice < 0) {
                            ui.notifications.warn('OD6S.NOT_ENOUGH_DICE');
                            return;
                        }
                        if (dice > 0) rollString = dice + 'd6' + game.i18n.localize('OD6S.BASE_DIE_FLAVOR');
                        rollString += '+1dw' + game.i18n.localize('OD6S.WILD_DIE_FLAVOR');
                    } else {
                        rollString = dice + 'd6' + game.i18n.localize('OD6S.BASE_DIE_FLAVOR');
                    }
                    if (pips > 0) rollString += '+' + pips;

                    let label = game.i18n.localize('OD6S.ROLLING');
                    if (damageRoll) {
                        label += " " + game.i18n.localize('OD6S.DAMAGE') + "(" +
                            game.i18n.localize(OD6S.damageTypes[damageType]) + ")";
                    }
                    let roll = await new Roll(rollString).evaluate({"async": true});

                    let flags = {
                        "type": "simple",
                        "wild": false,
                        "wildHandled": false,
                        "wildResult": OD6S.wildDieResult[OD6S.wildDieOneDefault],
                    };
                    if (damageRoll) {
                        flags = {
                            "type": "damage",
                            "source": game.i18n.localize('OD6S.DAMAGE'),
                            "damageType": damageType,
                            "isOpposable": true,
                            "wild": false,
                            "wildHandled": false,
                            "wildResult": OD6S.wildDieResult[OD6S.wildDieOneDefault],
                        }
                    }

                    if (game.settings.get('od6s', 'use_wild_die')) {
                        if (roll.terms.find(d => d.flavor === "Wild").total === 1) {
                            flags.wild = true;
                            if (OD6S.wildDieOneDefault > 0 && OD6S.wildDieOneAuto === 0) {
                                flags.wildHandled = true;
                            }
                        } else {
                            flags.wild = false;
                        }
                    }

                    if (game.user.isGM && game.settings.get('od6s', 'hide-gm-rolls')) rollMode = CONST.DICE_ROLL_MODES.PRIVATE;
                    let rollMessage = await roll.toMessage({
                        speaker: ChatMessage.getSpeaker(),
                        flavor: label,
                        flags: {
                            od6s: flags
                        },
                        rollMode: rollMode, create: true
                    });

                    if (flags.wild === true && OD6S.wildDieOneDefault === 2 && OD6S.wildDieOneAuto === 0) {
                        let replacementRoll = JSON.parse(JSON.stringify(rollMessage.roll.toJSON()));
                        let highest = 0;
                        for (let i = 0; i < replacementRoll.terms[0].results.length; i++) {
                            replacementRoll.terms[0].results[i].result >
                            replacementRoll.terms[0].results[highest].result ?
                                highest = i : {}
                        }
                        replacementRoll.terms[0].results[highest].discarded = true;
                        replacementRoll.terms[0].results[highest].active = false;
                        replacementRoll.total -= (+replacementRoll.terms[0].results[highest].result);
                        const rollMessageUpdate = {};
                        rollMessageUpdate.data = {};
                        rollMessageUpdate.content = replacementRoll.total;
                        rollMessageUpdate.id = rollMessage.id;
                        rollMessageUpdate.roll = replacementRoll;

                        if (game.user.isGM) {
                            if (rollMessage.getFlag('od6s', 'difficulty') && rollMessage.getFlag('od6s', 'success')) {
                                replacementRoll.total < rollMessage.getFlag('od6s', 'difficulty') ? await rollMessage.setFlag('od6s', 'success', false) :
                                    await rollMessage.setFlag('od6s', 'success', true);
                            }
                            await rollMessage.setFlag('od6s', 'originalroll', rollMessage.roll)
                            await rollMessage.update(rollMessageUpdate, {"diff": true});
                        } else {
                            game.socket.emit('system.od6s', {
                                operation: 'updateRollMessage',
                                message: rollMessage,
                                update: rollMessageUpdate
                            })
                        }
                    }
                }
            }
        }
    }).render(true);
}

export class WildDie extends Die {
    constructor(termData) {
        termData.faces = 6;
        termData.modifiers = ["x6"];
        super(termData);
    }

    static DENOMINATION = "w";
}

export class CharacterPointDie extends Die {
    constructor(termData) {
        termData.faces = 6;
        termData.modifiers = ["x6"];
        super(termData);
    }

    static DENOMINATION = "b";
}

Hooks.once("socketlib.ready", () => {
    OD6S.socket = socketlib.registerSystem("od6s");
    OD6S.socket.register("checkCrewStatus", checkCrewStatus);
    OD6S.socket.register("sendVehicleData", sendVehicleData);
    OD6S.socket.register("modifyShields", modifyShields);
    OD6S.socket.register("unlinkCrew", unlinkCrew);
    OD6S.socket.register("addToVehicle", addToVehicle);
    OD6S.socket.register("updateVehicle", updateVehicle);
});

/**
 * Check is an actor is crewing a vehicle
 * @param actorId
 * @returns {boolean|*}
 */
async function checkCrewStatus(actorId) {
    const actor = await od6sutilities.getActorFromUuid(actorId);
    return actor.isCrewMember();
}

/**
 * Update actor's vehicle data
 * @param data
 */
async function sendVehicleData(data) {
    for (const e of data.crewmembers) {
        let actor = await od6sutilities.getActorFromUuid(e.uuid);
        const update = {};
        update.id = actor.id;
        update.vehicle = data;
        await actor.update(update);
    }
}

/**
 * Update vehicle's shields from actor
 * @param update
 * @returns {Promise<void>}
 */
async function modifyShields(update) {
    const actor = await od6sutilities.getActorFromUuid(update.uuid);
    await actor.update(update);
}

/**
 * Remove crewmwmber
 * @param actorId
 * @param crewId
 * @returns {Promise<void>}
 */
async function unlinkCrew(vehicle, crew) {
    const actor = await od6sutilities.getActorFromUuid(crew);
    await actor.sheet.unlinkCrew(vehicle);
}

/**
 * Add crewmember
 * @param vehicleId
 * @param crewId
 * @returns {Promise<void>}
 */
async function addToVehicle(vehicleId, crewId) {
    const actor = await od6sutilities.getActorFromUuid(crewId);
    return await actor.addToCrew(vehicleId);
}

/**
 * Update a vehicle
 * @param vehicleID
 * @param update
 * @returns {Promise<*>}
 */
async function updateVehicle(vehicleID, update) {
    const actor = await od6sutilities.getActorFromUuid(vehicleID);
    return await actor.update(update);
}

export async function getActorFromUuid(uuid) {
    return od6sutilities.getActorFromUuid(uuid);
}
