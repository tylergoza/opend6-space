import OD6S from "../config/config-od6s.js"

export default class od6sCustomFieldsConfiguration extends FormApplication {
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "custom_labels";
        options.template = "systems/od6s/templates/settings/custom-fields.html";
        options.width = 600;
        options.minimizable = true;
        options.resizable = true;
        options.title = game.i18n.localize("OD6S.CONFIG_CUSTOM_FIELDS");
        return options;
    }

    getData() {
        let data = super.getData;

        data.settings = Array.from(game.settings.settings).filter(s => s[1].od6sCustomField).map(i => i[1])
        data.settings.forEach(s => s.inputType = s.type == Boolean ? "checkbox" : "text")
        data.settings.forEach(s => s.choice = typeof(s.choices) === 'undefined'  ? false : true)
        data.settings.forEach(s => s.value = game.settings.get(s.namespace, s.key))
        return data;
    }

    async _updateObject(event, formData) {
        for(let setting in formData) {
            if (setting.includes("actor_types")) {
                let value = formData[setting][0];
                for(let type in OD6S.actorMasks) {
                    value = formData[setting].includes(type) ?
                        this.updateActorTypes(value,type,true) : this.updateActorTypes(value,type,false);
                }
                await game.settings.set("od6s", setting, value);
            } else {
                await game.settings.set("od6s", setting, formData[setting]);
            }
        }
    }

    updateActorTypes(value, type, op) {
        if (op) {
            value |= (1 << OD6S.actorMasks[type]);
        } else {
            value &= ~(1 << OD6S.actorMasks[type]);
        }
        return value;
    }
}