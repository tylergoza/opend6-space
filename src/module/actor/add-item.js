import {od6sutilities} from "../system/utilities.js";

export class OD6SAddItem extends FormApplication {
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "add-item";
        options.template = "systems/od6s/templates/actor/common/add-item.html";
        options.height = 200;
        options.width = 300;
        options.minimizable = true;
        options.title = game.i18n.localize("OD6S.ADD");
        return options;
    }

    getData() {
        return super.getData();
    }

    async _updateObject(ev, formData) {
        if (ev.submitter.value === 'cancel') {
            return;
        }

        const actor = await od6sutilities.getActorFromUuid(formData.actor);

        if (ev.submitter.value === 'selected') {
            const items = JSON.parse(formData.serializeditems);
            await actor.createEmbeddedDocuments('Item', [items[formData['add-item']]]);
        }

        if (ev.submitter.value === 'empty') {
            const itemData = {};
            itemData.type = formData.type;
            if(itemData.type === 'skill') {
                itemData.data = {};
                itemData.data.attribute = formData.attrname;
            }
            itemData.name = game.i18n.localize('OD6S.NEW_ITEM');
            const item = await new Item(itemData);
            await actor.createEmbeddedDocuments('Item', [item.toObject()]);
        }
    }
}

export default OD6SAddItem;