// System Settings
import OD6S from "./config-od6s.js";
import od6sCustomLabelsConfiguration from "../apps/config-labels.js";
import od6sWildDieConfiguration from "../apps/config-wild-die.js";
import od6sDeadlinessConfiguration from "../apps/config-deadliness.js";
import od6sRevealConfiguration from "../apps/config-reveal.js";
import od6sRulesConfiguration from "../apps/config-rules.js";
import od6sDifficultyConfiguration from "../apps/config-difficulty.js"
import od6sCustomFieldsConfiguration from "../apps/config-custom-fields.js"
import od6sAutomationConfiguration from "../apps/config-automation.js"

export default function od6sSettings() {
    Hooks.once('init', async function () {

        game.settings.registerMenu("od6s", "custom_labels_menu", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_LABELS"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_LABELS_DESCRIPTION"),
            label: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_LABELS"),
            type: od6sCustomLabelsConfiguration,
            restricted: true
        })

        game.settings.registerMenu("od6s", "custom_fields_menu", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_FIELDS"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_FIELDS_DESCRIPTION"),
            label: game.i18n.localize("OD6S.CONFIG_CUSTOM_FIELDS"),
            type: od6sCustomFieldsConfiguration,
            restricted: true
        })

        game.settings.registerMenu("od6s", "wild_die_menu", {
            name: game.i18n.localize("OD6S.CONFIG_WILD_DIE_MENU"),
            hint: game.i18n.localize("OD6S.CONFIG_WILD_DIE_MENU_DESCRIPTION"),
            label: game.i18n.localize("OD6S.CONFIG_WILD_DIE_MENU_LABEL"),
            type: od6sWildDieConfiguration,
            restricted: true
        })

        game.settings.registerMenu("od6s", "deadliness_menu", {
            name: game.i18n.localize("OD6S.CONFIG_DEADLINESS_MENU"),
            hint: game.i18n.localize("OD6S.CONFIG_DEADLINESS_MENU_DESCRIPTION"),
            label: game.i18n.localize("OD6S.CONFIG_DEADLINESS_MENU"),
            type: od6sDeadlinessConfiguration,
            restricted: true
        })

        game.settings.registerMenu("od6s", "reveal_menu", {
            name: game.i18n.localize("OD6S.CONFIG_REVEAL_MENU"),
            hint: game.i18n.localize("OD6S.CONFIG_REVEAL_MENU_DESCRIPTION"),
            label: game.i18n.localize("OD6S.CONFIG_REVEAL_MENU_LABEL"),
            type: od6sRevealConfiguration,
            restricted: true
        })

        game.settings.registerMenu("od6s", "rules_options_menu", {
            name: game.i18n.localize("OD6S.CONFIG_RULES_OPTIONS_MENU"),
            hint: game.i18n.localize("OD6S.CONFIG_RULES_OPTIONS_MENU_DESCRIPTION"),
            label: game.i18n.localize("OD6S.CONFIG_RULES_OPTIONS_MENU_LABEL"),
            type: od6sRulesConfiguration,
            restricted: true
        })

        game.settings.registerMenu("od6s", "automation_menu", {
            name: game.i18n.localize("OD6S.CONFIG_AUTOMATION_OPTIONS_MENU"),
            hint: game.i18n.localize("OD6S.CONFIG_AUTOMATION_OPTIONS_MENU_DESCRIPTION"),
            label: game.i18n.localize("OD6S.CONFIG_AUTOMATION_OPTIONS_MENU_LABEL"),
            type: od6sAutomationConfiguration,
            restricted: true
        })

        game.settings.registerMenu("od6s", "difficulty_menu", {
            name: game.i18n.localize("OD6S.CONFIG_DIFFICULTY_MENU"),
            hint: game.i18n.localize("OD6S.CONFIG_DIFFICULTY_MENU_DESCRIPTION"),
            label: game.i18n.localize("OD6S.CONFIG_DIFFICULTY_MENU_LABEL"),
            type: od6sDifficultyConfiguration,
            restricted: true
        })

        game.settings.register("od6s", "auto_opposed", {
            name: game.i18n.localize("OD6S.CONFIG_AUTO_OPPOSED"),
            hint: game.i18n.localize("OD6S.CONFIG_AUTO_OPPOSED_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sAutomation: true,
            type: Boolean,
            default: false,
            onChange: value => (value ? OD6S.autoOpposed = value : true)
        })

        game.settings.register("od6s", "customize_fate_points", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_FATE_POINTS"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_FATE_POINTS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: '',
            onChange: value => (value ? OD6S.fatePointsName = value : game.i18n.localize('OD6S.CHAR_FATE_POINTS'))
        })

        game.settings.register("od6s", "customize_fate_points_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_FATE_POINTS_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_FATE_POINTS_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.fatePointsShortName = value : game.i18n.localize('OD6S.CHAR_FATE_POINTS_SHORT'))
        })

        game.settings.register("od6s", "customize_use_a_fate_point", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_USE_FATE_POINT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_USE_FATE_POINT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.useAFatePointName = value : game.i18n.localize('OD6S.CHAR_USE_FATE_POINT'))
        })

        game.settings.register("od6s", "custom_field_1", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String
        })

        game.settings.register("od6s", "custom_field_1_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String
        })

        game.settings.register("od6s", "custom_field_1_type", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_TYPE"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_TYPE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String,
            choices: {
                "number": game.i18n.localize("OD6S.NUMBER"),
                "string": game.i18n.localize("OD6S.STRING")
            }
        })

        game.settings.register("od6s", "custom_field_1_actor_types", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_ACTOR_TYPES"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_ACTOR_TYPES_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            actorType: true,
            default: 1,
            type: Number
        })

        game.settings.register("od6s", "custom_field_2", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_2"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_2_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String
        })

        game.settings.register("od6s", "custom_field_2_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_2_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_2_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String
        })

        game.settings.register("od6s", "custom_field_2_type", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_2_TYPE"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_2_TYPE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String,
            choices: {
                "number": game.i18n.localize("OD6S.NUMBER"),
                "string": game.i18n.localize("OD6S.STRING")
            }
        })

        game.settings.register("od6s", "custom_field_2_actor_types", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_ACTOR_TYPES"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_ACTOR_TYPES_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            actorType: true,
            default: 1,
            type: Number
        })

        game.settings.register("od6s", "custom_field_3", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_3"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_3_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String
        })

        game.settings.register("od6s", "custom_field_3_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_3_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_3_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String
        })

        game.settings.register("od6s", "custom_field_3_type", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_3_TYPE"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_3_TYPE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String,
            choices: {
                "number": game.i18n.localize("OD6S.NUMBER"),
                "string": game.i18n.localize("OD6S.STRING")
            }
        })

        game.settings.register("od6s", "custom_field_3_actor_types", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_ACTOR_TYPES"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_ACTOR_TYPES_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            actorType: true,
            default: 1,
            type: Number
        })

        game.settings.register("od6s", "custom_field_4", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_4"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_4_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String
        })

        game.settings.register("od6s", "custom_field_4_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_4_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_4_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String
        })

        game.settings.register("od6s", "custom_field_4_type", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_4_TYPE"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_4_TYPE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            default: "",
            type: String,
            choices: {
                "number": game.i18n.localize("OD6S.NUMBER"),
                "string": game.i18n.localize("OD6S.STRING")
            }
        })

        game.settings.register("od6s", "custom_field_4_actor_types", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_ACTOR_TYPES"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_ACTOR_TYPES_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sCustomField: true,
            actorType: true,
            default: 1,
            type: Number
        })

        game.settings.register("od6s", "customize_currency_label", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_CURRENCY_LABEL"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_CURRENCY_LABEL_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            default: "",
            type: String,
            onChange: value => (value ? OD6S.currencyName = value : game.i18n.localize('OD6S.CREDITS'))
        })

        game.settings.register("od6s", "customize_vehicle_toughness", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_VEHICLE_TOUGHNESS"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_VEHICLE_TOUGHNESS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            default: "",
            type: String,
            onChange: value => (value ? OD6S.vehicle_toughnessName = value : game.i18n.localize('OD6S.TOUGHNESS'))
        })

        game.settings.register("od6s", "customize_starship_toughness", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STARSHIP_TOUGHNESS"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STARSHIP_TOUGHNESS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            default: "",
            type: String,
            onChange: value => (value ? OD6S.starship_toughnessName = value : game.i18n.localize('OD6S.TOUGHNESS'))
        })

        game.settings.register("od6s", "interstellar_drive_name", {
            name: game.i18n.localize('OD6S.CONFIG_INTERSTELLAR_DRIVE_NAME'),
            hint: game.i18n.localize('OD6S.CONFIG_INTERSTELLAR_DRIVE_NAME_DESCRIPTION'),
            scope: "world",
            config: false,
            default: "Interstellar Drive",
            type: String
        })

        game.settings.register("od6s", "customize_manifestations", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MANIFESTATIONS"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MANIFESTATIONS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            default: "",
            type: String,
            onChange: value => (value ? OD6S.manifestationsName = value : game.i18n.localize('OD6S.CHAR_MANIFESTATIONS'))
        })

        game.settings.register("od6s", "customize_manifestation", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MANIFESTATION"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MANIFESTATION_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            default: "",
            type: String,
            onChange: value => (value ? OD6S.manifestationName = value : game.i18n.localize('OD6S.CHAR_MANIFESTATIONS'))
        })

        game.settings.register("od6s", "customize_metaphysics_name", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_NAME"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_NAME_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.attributes.met.name = value : game.i18n.localize('OD6S.CHAR_METAPHYSICS'))
        })

        game.settings.register("od6s", "customize_metaphysics_name_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_NAME_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_NAME_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.attributes.met.name = value : game.i18n.localize('OD6S.CHAR_METAPHYSICS'))
        })

        game.settings.register("od6s", "customize_metaphysics_extranormal", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_EXTRANORMAL"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_EXTRANORMAL_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            default: "",
            type: String,
            onChange: value => (value ? OD6S.metaphysicsExtranormalName = value : game.i18n.localize('OD6S.CHAR_METAPHYSICS_EXTRANORMAL'))
        })

        game.settings.register("od6s", "customize_metaphysics_skill_channel", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_SKILL_CHANNEL"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_SKILL_CHANNEL_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            default: "",
            type: String,
            onChange: value => (value ? OD6S.channelSkillName = value : game.i18n.localize('OD6S.METAPHYSICS_SKILL_CHANNEL'))
        })

        game.settings.register("od6s", "customize_metaphysics_skill_sense", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_SKILL_SENSE"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_SKILL_SENSE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            default: "",
            type: String,
            onChange: value => (value ? OD6S.senseSkillName = value : game.i18n.localize('OD6S.METAPHYSICS_SKILL_SENSE'))
        })

        game.settings.register("od6s", "customize_metaphysics_skill_transform", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_SKILL_TRANSFORM"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_SKILL_TRANSFORM_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            default: "",
            type: String,
            onChange: value => (value ? OD6S.transformSkillName = value : game.i18n.localize('OD6S.METAPHYSICS_SKILL_TRANSFORM'))
        })

        game.settings.register("od6s", "customize_agility_name", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_AGILITY_NAME"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_AGILITY_NAME_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: '',
            onChange: value => (value ? OD6S.attributes.agi.name = value : game.i18n.localize('OD6S.CHAR_AGILITY'))
        })

        game.settings.register("od6s", "customize_agility_name_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_AGILITY_NAME_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_AGILITY_NAME_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.attributes.agi.shortName = value : game.i18n.localize('OD6S.CHAR_AGILITY_SHORT'))
        })

        game.settings.register("od6s", "customize_strength_name", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STRENGTH_NAME"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STRENGTH_NAME_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: '',
            onChange: value => (value ? OD6S.attributes.str.name = value : game.i18n.localize('OD6S.CHAR_STRENGTH'))
        })

        game.settings.register("od6s", "customize_strength_name_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STRENGTH_NAME_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STRENGTH_NAME_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.attributes.str.shortName = value : game.i18n.localize('OD6S.CHAR_STRENGTH_SHORT'))
        })

        game.settings.register("od6s", "customize_mechanical_name", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MECHANICAL_NAME"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MECHANICAL_NAME_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: '',
            onChange: value => (value ? OD6S.attributes.mec.name = value : game.i18n.localize('OD6S.CHAR_MECHANICAL'))
        })

        game.settings.register("od6s", "customize_mechanical_name_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MECHANICAL_NAME_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MECHANICAL_NAME_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.attributes.mec.shortName = value : game.i18n.localize('OD6S.CHAR_MECHANICAL_SHORT'))
        })

        game.settings.register("od6s", "customize_knowledge_name", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_KNOWLEDGE_NAME"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_KNOWLEDGE_NAME_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: '',
            onChange: value => (value ? OD6S.attributes.kno.name = value : game.i18n.localize('OD6S.CHAR_KNOWLEDGE'))
        })

        game.settings.register("od6s", "customize_knowledge_name_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_KNOWLEDGE_NAME_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_KNOWLEDGE_NAME_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.attributes.kno.shortName = value : game.i18n.localize('OD6S.CHAR_KNOWLEDGE_SHORT'))
        })

        game.settings.register("od6s", "customize_perception_name", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_PERCEPTION_NAME"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_PERCEPTION_NAME_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: '',
            onChange: value => (value ? OD6S.attributes.per.name = value : game.i18n.localize('OD6S.CHAR_PERCEPTION'))
        })

        game.settings.register("od6s", "customize_perception_name_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_PERCEPTION_NAME_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_PERCEPTION_NAME_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.attributes.per.shortName = value : game.i18n.localize('OD6S.CHAR_PERCEPTION_NAME'))
        })

        game.settings.register("od6s", "customize_technical_name", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_TECHNICAL_NAME"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_TECHNICAL_NAME_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: '',
            onChange: value => (value ? OD6S.attributes.tec.name = value : game.i18n.localize('OD6S.CHAR_TECHNICAL'))
        })

        game.settings.register("od6s", "customize_technical_name_short", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_TECHNICAL_NAME_SHORT"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_TECHNICAL_NAME_SHORT_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.attributes.tec.shortName = value : game.i18n.localize('OD6S.CHAR_TECHNICAL_NAME'))
        })

        game.settings.register("od6s", "customize_body_points_name", {
            name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_BODY_POINTS_NAME"),
            hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_BODY_POINTS_NAME_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sLabel: true,
            type: String,
            default: "",
            onChange: value => (value ? OD6S.bodyPointsName = value : game.i18n.localize('OD6S.BODY_POINTS'))
        })


        game.settings.register("od6s", "hide_compendia", {
            name: game.i18n.localize("OD6S.CONFIG_HIDE_COMPENDIA"),
            hint: game.i18n.localize("OD6S.CONFIG_HIDE_COMPENDIA_DESCRIPTION"),
            scope: "world",
            config: true,
            default: false,
            type: Boolean,
            onChange: () => {
                game.compendiumDirectory.render();
            }
        })

        game.settings.register("od6s", "deadliness", {
            name: game.i18n.localize("OD6S.CONFIG_DEADLINESS"),
            hint: game.i18n.localize("OD6S.CONFIG_DEADLINESS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sDeadliness: true,
            default: 3,
            type: Number,
            choices: {
                "1": game.i18n.localize("OD6S.CONFIG_DEADLINESS_1"),
                "2": game.i18n.localize("OD6S.CONFIG_DEADLINESS_2"),
                "3": game.i18n.localize("OD6S.CONFIG_DEADLINESS_3"),
                "4": game.i18n.localize("OD6S.CONFIG_DEADLINESS_4"),
                "5": game.i18n.localize("OD6S.CONFIG_DEADLINESS_5")
            },
            onChange: value => (OD6S.deadlinessLevel["character"] = value)
        })

        game.settings.register("od6s", "npc-deadliness", {
            name: game.i18n.localize("OD6S.CONFIG_NPC_DEADLINESS"),
            hint: game.i18n.localize("OD6S.CONFIG_NPC_DEADLINESS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sDeadliness: true,
            default: 4,
            type: Number,
            choices: {
                "1": game.i18n.localize("OD6S.CONFIG_DEADLINESS_1"),
                "2": game.i18n.localize("OD6S.CONFIG_DEADLINESS_2"),
                "3": game.i18n.localize("OD6S.CONFIG_DEADLINESS_3"),
                "4": game.i18n.localize("OD6S.CONFIG_DEADLINESS_4"),
                "5": game.i18n.localize("OD6S.CONFIG_DEADLINESS_5")
            },
            onChange: value => (OD6S.deadlinessLevel["npc"] =  value)
        })

        game.settings.register("od6s", "creature-deadliness", {
            name: game.i18n.localize("OD6S.CONFIG_CREATURE_DEADLINESS"),
            hint: game.i18n.localize("OD6S.CONFIG_CREATURE_DEADLINESS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sDeadliness: true,
            default: 4,
            type: Number,
            choices: {
                "1": game.i18n.localize("OD6S.CONFIG_DEADLINESS_1"),
                "2": game.i18n.localize("OD6S.CONFIG_DEADLINESS_2"),
                "3": game.i18n.localize("OD6S.CONFIG_DEADLINESS_3"),
                "4": game.i18n.localize("OD6S.CONFIG_DEADLINESS_4"),
                "5": game.i18n.localize("OD6S.CONFIG_DEADLINESS_5")
            },
            onChange: value => (OD6S.deadlinessLevel["creature"] =  value)
        })
        
        game.settings.register("od6s", "hide-skill-cards", {
            name: game.i18n.localize("OD6S.CONFIG_HIDE_SKILL_ROLLS"),
            hint: game.i18n.localize("OD6S.CONFIG_HIDE_SKILL_ROLLS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sReveal: true,
            type: Boolean,
            default: true
        })

        game.settings.register("od6s", "hide-combat-cards", {
            name: game.i18n.localize("OD6S.CONFIG_HIDE_ATTACK_ROLLS"),
            hint: game.i18n.localize("OD6S.CONFIG_HIDE_ATTACK_ROLLS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sReveal: true,
            type: Boolean,
            default: true
        })

        game.settings.register("od6s", "roll-modifiers", {
            name: game.i18n.localize("OD6S.CONFIG_SHOW_MODIFIERS"),
            hint: game.i18n.localize("OD6S.CONFIG_SHOW_MODIFIERS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sReveal: true,
            type: Boolean,
            default: true
        })

        game.settings.register("od6s", "hide-gm-rolls", {
            name: game.i18n.localize("OD6S.CONFIG_HIDE_GM_ROLLS"),
            hint: game.i18n.localize("OD6S.CONFIG_HIDE_GM_ROLLS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sReveal: true,
            type: Boolean,
            default: true
        })

        game.settings.register("od6s", "use_wild_die", {
            name: game.i18n.localize("OD6S.CONFIG_USE_WILD_DIE"),
            hint: game.i18n.localize("OD6S.CONFIG_USE_WILD_DIE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sWildDie: true,
            type: Boolean,
            default: true
        })

        game.settings.register("od6s", "default_wild_one", {
            name: game.i18n.localize("OD6S.CONFIG_WILD_DIE_ONE"),
            hint: game.i18n.localize("OD6S.CONFIG_WILD_DIE_ONE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sWildDie: true,
            default: 0,
            type: Number,
            choices: {
                "0": game.i18n.localize("OD6S.CONFIG_WILD_DIE_0"),
                "1": game.i18n.localize("OD6S.CONFIG_WILD_DIE_1"),
                "2": game.i18n.localize("OD6S.CONFIG_WILD_DIE_2"),
                "3": game.i18n.localize("OD6S.CONFIG_WILD_DIE_3")
            },
            onChange: value => (OD6S.wildDieOneDefault = value)
        })

        game.settings.register("od6s", "default_wild_die_one_handle", {
            name: game.i18n.localize("OD6S.CONFIG_WILD_DIE_ONE_HANDLE"),
            hint: game.i18n.localize("OD6S.CONFIG_WILD_DIE_ONE_HANDLE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sWildDie: true,
            default: 0,
            type: Number,
            choices: {
                "0": game.i18n.localize("OD6S.CONFIG_WILD_DIE_HANDLE_0"),
                "1": game.i18n.localize("OD6S.CONFIG_WILD_DIE_HANDLE_1")
            },
            onChange: value => (OD6S.wildDieOneAuto = value)
        })

        game.settings.register("od6s", "wild_die_one_face", {
            name: game.i18n.localize('OD6S.CONFIG_WILD_DIE_ONE_FACE'),
            hint: game.i18n.localize("OD6S.CONFIG_WILD_DIE_ONE_FACE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sWildDie: true,
            default: "systems/od6s/icons/skull-shield.png",
            type: String,
            filePicker: "image"
        })

        game.settings.register("od6s", "wild_die_six_face", {
            name: game.i18n.localize('OD6S.CONFIG_WILD_DIE_SIX_FACE'),
            hint: game.i18n.localize("OD6S.CONFIG_WILD_DIE_SIX_FACE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sWildDie: true,
            default: "systems/od6s/icons/eclipse-flare.png",
            type: String,
            filePicker: "image"
        })

        game.settings.register("od6s", "bodypoints", {
            name: game.i18n.localize("OD6S.CONFIG_USE_BODY"),
            hint: game.i18n.localize("OD6S.CONFIG_USE_BODY_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            default: 0,
            type: Number,
            choices: {
                "0": game.i18n.localize("OD6S.CONFIG_USE_WOUNDS"),
                "1": game.i18n.localize("OD6S.CONFIG_USE_WOUNDS_WITH_BODY"),
                "2": game.i18n.localize("OD6S.CONFIG_USE_BODY_ONLY")
            },
            onChange: value => (OD6S.woundConfig = value)
        })

        game.settings.register("od6s", "highhitdamage", {
            name: game.i18n.localize("OD6S.CONFIG_USE_OPTIONAL_DAMAGE"),
            hint: game.i18n.localize("OD6S.CONFIG_USE_OPTIONAL_DAMAGE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean,
            onChange: value => (OD6S.highHitDamage = value)
        })

        /* TODO

                game.settings.register("od6s", "initoptions", {
                  name:  game.i18n.localize("OD6S.CONFIG_INITIATIVE_SETTING"),
                  hint:  game.i18n.localize("OD6S.CONFIG_INITIATIVE_SETTING_DESCRIPTION"),
                  scope: "world",
                  config: true,
                  default: 1,
                  type: Number,
                  choices: {
                    "0":  game.i18n.localize("OD6S.CONFIG_INITIATIVE_1"),
                    "1":  game.i18n.localize("OD6S.CONFIG_INITIATIVE_2"),
                    "2":  game.i18n.localize("OD6S.CONFIG_INITIATIVE_3"),
                    "3":  game.i18n.localize("OD6S.CONFIG_INITIATIVE_4")
                  }
                })

                game.settings.register("od6s", "initbonus", {
                  name:  game.i18n.localize("OD6S.CONFIG_USE_INIT_BONUS"),
                  hint:  game.i18n.localize("OD6S.CONFIG_USE_INIT_BONUS_DESCRIPTION"),
                  scope: "world",
                  config: true,
                  default: false,
                  type: Boolean
                })

                game.settings.register("od6s", "fastcombat", {
                  name:  game.i18n.localize("OD6S.CONFIG_FAST_COMBAT"),
                  hint:  game.i18n.localize("OD6S.CONFIG_FAST_COMBAT_DESCRIPTION"),
                  scope: "world",
                  config: true,
                  default: false,
                  type: Boolean
                })
                */

        game.settings.register("od6s", "hide_advantages_disadvantages", {
            name: game.i18n.localize("OD6S.CONFIG_HIDE_ADVANTAGES_DISADVANTAGES"),
            hint: game.i18n.localize("OD6S.CONFIG_HIDE_ADVANTAGES_DISADVANTAGES_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean,
            onChange: () => {
                game.compendiumDirectory.render();
            }
        })

        game.settings.register("od6s", "brawl_attribute", {
            name: game.i18n.localize("OD6S.CONFIG_BRAWL_ATTRIBUTE"),
            hint: game.i18n.localize("OD6S.CONFIG_BRAWL_ATTRIBUTE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            default: "agi",
            type: String,
            choices: {
                "agi": game.i18n.localize("OD6S.CHAR_AGILITY"),
                "str": game.i18n.localize("OD6S.CHAR_STRENGTH"),
            }
        })

        game.settings.register("od6s", "parry_skills", {
            name: game.i18n.localize("OD6S.CONFIG_PARRY_SKILLS"),
            hint: game.i18n.localize("OD6S.CONFIG_PARRY_SKILLS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            type: Boolean,
            default: false
        })

        game.settings.register("od6s", "reaction_skills", {
            name: game.i18n.localize("OD6S.CONFIG_REACTION_SKILLS"),
            hint: game.i18n.localize("OD6S.CONFIG_REACTION_SKILLS_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            type: Boolean,
            default: false
        })

        game.settings.register("od6s", "defense_lock", {
            name: game.i18n.localize("OD6S.CONFIG_DEFENSE_LOCK"),
            hint: game.i18n.localize("OD6S.CONFIG_DEFENSE_LOCK_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            type: Boolean,
            default: false,
            onChange: value => OD6S.defenseLock = value
        })

        game.settings.register("od6s", "fate_point_round", {
            name: game.i18n.localize("OD6S.CONFIG_FATE_POINT_ROUND"),
            hint: game.i18n.localize("OD6S.CONFIG_FATE_POINT_ROUND_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            type: Boolean,
            default: false,
            onChange: value => OD6S.fatePointRound = value
        })

        game.settings.register("od6s", "fate_point_climactic", {
            name: game.i18n.localize("OD6S.CONFIG_FATE_POINT_CLIMACTIC"),
            hint: game.i18n.localize("OD6S.CONFIG_FATE_POINT_CLIMACTIC_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            type: Boolean,
            default: false,
            onChange: value => OD6S.fatePointClimactic = value
        })

        game.settings.register("od6s", "strength_damage", {
            name: game.i18n.localize("OD6S.CONFIG_STRENGTH_DAMAGE"),
            hint: game.i18n.localize("OD6S.CONFIG_STRENGTH_DAMAGE_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            type: Boolean,
            default: false
        })

        game.settings.register("od6s", "metaphysics_attribute_optional", {
            name: game.i18n.localize("OD6S.CONFIG_METAPHYSICS_ATTRIBUTE_OPTIONAL"),
            hint: game.i18n.localize("OD6S.CONFIG_METAPHYSICS_ATTRIBUTE_OPTIONAL_DESCRIPTION"),
            scope: "world",
            config: false,
            od6sRules: true,
            type: Boolean,
            default: false
        })

        game.settings.register("od6s", "dice_for_scale", {
            name: game.i18n.localize('OD6S.CONFIG_DICE_FOR_SCALE'),
            hint: game.i18n.localize('OD6S.CONFIG_DICE_FOR_SCALE_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean
        })

        game.settings.register("od6s", "sensors", {
            name: game.i18n.localize('OD6S.CONFIG_SENSORS'),
            hint: game.i18n.localize('OD6S.CONFIG_SENSORS_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean
        })

        game.settings.register("od6s", "vehicle_difficulty", {
            name: game.i18n.localize('OD6S.CONFIG_VEHICLE_DIFFICULTY'),
            hint: game.i18n.localize('OD6S.CONFIG_VEHICLE_DIFFICULTY_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: true,
            type: Boolean,
            onChange: value => OD6S.vehicleDifficulty = value
        })

        game.settings.register("od6s", "passenger_damage_dice", {
            name: game.i18n.localize('OD6S.CONFIG_PASSENGER_DAMAGE_DICE'),
            hint: game.i18n.localize('OD6S.CONFIG_PASSENGER_DAMAGE_DICE_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean,
            onChange: value => OD6S.passengerDamageDice = value
        })

        game.settings.register("od6s", "dice_for_grenades", {
            name: game.i18n.localize('OD6S.CONFIG_GRENADE_DAMAGE_DICE'),
            hint: game.i18n.localize('OD6S.CONFIG_GRENADE_DAMAGE_DICE_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean,
            onChange: value => OD6S.grenadeDamageDice = value
        })

        game.settings.register("od6s", "map_range_to_difficulty", {
            name: game.i18n.localize('OD6S.CONFIG_MAP_RANGE_TO_DIFFICULTY'),
            hint: game.i18n.localize('OD6S.CONFIG_MAP_RANGE_TO_DIFFICULTY_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean,
            onChange: value => OD6S.mapRange = value
        })

        game.settings.register("od6s", "melee_difficulty", {
            name: game.i18n.localize('OD6S.CONFIG_MELEE_DIFFICULTY'),
            hint: game.i18n.localize('OD6S.CONFIG_MELEE_DIFFICULTY_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean,
            onChange: value => OD6S.meleeDifficulty = value
        })

        game.settings.register("od6s", "cost", {
            name: game.i18n.localize('OD6S.CONFIG_COST'),
            hint: game.i18n.localize('OD6S.CONFIG_COST_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: "1",
            type: String,
            choices: {
                "0": game.i18n.localize("OD6S.CONFIG_COST_PRICE"),
                "1": game.i18n.localize("OD6S.CONFIG_COST_COST"),
            },
            onChange: value => OD6S.cost = value
        })

        game.settings.register("od6s", "funds_fate", {
            name: game.i18n.localize('OD6S.CONFIG_FUNDS_FATE'),
            hint: game.i18n.localize('OD6S.CONFIG_FUNDS_FATE'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean,
            onChange: value => OD6S.fundsWild = value
        })

        game.settings.register("od6s", "random_hit_locations", {
            name: game.i18n.localize('OD6S.CONFIG_RANDOM_HIT_LOCATIONS'),
            hint: game.i18n.localize('OD6S.CONFIG_RANDOM_HIT_LOCATIONS_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean,
            onChange: value => OD6S.randomHitLocations = value
        })

        game.settings.register("od6s", "pip_per_dice", {
            name: game.i18n.localize('OD6S.CONFIG_PIP_PER_DICE'),
            hint: game.i18n.localize('OD6S.CONFIG_PIP_PER_DICE_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: 3,
            type: Number,
            onChange: value => OD6S.pipsPerDice = value
        })

        game.settings.register("od6s", "flat_skills", {
            name: game.i18n.localize('OD6S.CONFIG_FLAT_SKILLS'),
            hint: game.i18n.localize('OD6S.CONFIG_FLAT_SKILLS_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sRules: true,
            default: false,
            type: Boolean,
            onChange: value => OD6S.flatSkills = value
        })

        game.settings.register('od6s', 'default_difficulty_very_easy', {
            name: game.i18n.localize('OD6S.DIFFICULTY_VERY_EASY'),
            scope: "world",
            config: false,
            od6sDifficulty: true,
            default: 5,
            type: Number,
            onChange: value => OD6S.difficulty["OD6S.DIFFICULTY_VERY_EASY"].max = value
        })

        game.settings.register('od6s', 'default_difficulty_easy', {
            name: game.i18n.localize('OD6S.DIFFICULTY_EASY'),
            scope: "world",
            config: false,
            od6sDifficulty: true,
            default: 10,
            type: Number,
            onChange: value => OD6S.difficulty["OD6S.DIFFICULTY_EASY"].max = value
        })

        game.settings.register('od6s', 'default_difficulty_moderate', {
            name: game.i18n.localize('OD6S.DIFFICULTY_MODERATE'),
            scope: "world",
            config: false,
            od6sDifficulty: true,
            default: 15,
            type: Number,
            onChange: value => OD6S.difficulty["OD6S.DIFFICULTY_MODERATE"].max = value
        })

        game.settings.register('od6s', 'default_difficulty_difficult', {
            name: game.i18n.localize('OD6S.DIFFICULTY_DIFFICULT'),
            scope: "world",
            config: false,
            od6sDifficulty: true,
            default: 20,
            type: Number,
            onChange: value => OD6S.difficulty["OD6S.DIFFICULTY_DIFFICULT"].max = value
        })

        game.settings.register('od6s', 'default_difficulty_very_difficult', {
            name: game.i18n.localize('OD6S.DIFFICULTY_VERY_DIFFICULT'),
            scope: "world",
            config: false,
            od6sDifficulty: true,
            default: 25,
            type: Number,
            onChange: value => OD6S.difficulty["OD6S.DIFFICULTY_VERY_DIFFICULT"].max = value
        })

        game.settings.register('od6s', 'default_difficulty_heroic', {
            name: game.i18n.localize('OD6S.DIFFICULTY_HEROIC'),
            scope: "world",
            config: false,
            od6sDifficulty: true,
            default: 30,
            type: Number,
            onChange: value => OD6S.difficulty["OD6S.DIFFICULTY_HEROIC"].max = value
        })

        game.settings.register('od6s', 'default_difficulty_legendary', {
            name: game.i18n.localize('OD6S.DIFFICULTY_LEGENDARY'),
            scope: "world",
            config: false,
            od6sDifficulty: true,
            default: 40,
            type: Number,
            onChange: value => OD6S.difficulty["OD6S.DIFFICULTY_LEGENDARY"].max = value
        })

        game.settings.register('od6s', 'default_attack_difficulty', {
            name: game.i18n.localize('OD6S.CONFIG_ATTACK_DIFFICULTY'),
            hint: game.i18n.localize('OD6S.CONFIG_ATTACK_DIFFICULTY_DESCRIPTION'),
            scope: "world",
            config: false,
            od6sDifficulty: true,
            default: 10,
            type: Number,
            onChange: value => OD6S.difficulty["OD6S.DIFFICULTY_LEGENDARY"].max = value
        })

        game.settings.register("od6s", "highlight_effects", {
            name: game.i18n.localize('OD6S.CONFIG_HIGHLIGHT_EFFECTS'),
            hint: game.i18n.localize('OD6S.CONFIG_HIGHLIGHT_EFFECTS_DESCRIPTION'),
            scope: "world",
            config: true,
            default: false,
            type: Boolean,
            onChange: value => OD6S.highlightEffects = value
        })

        game.settings.register("od6s", "show_skill_specialization", {
            name: game.i18n.localize('OD6S.CONFIG_SHOW_SKILL_SPECIALIZATION'),
            hint: game.i18n.localize('OD6S.CONFIG_SHOW_SKILL_SPECIALIZATION_DESCRIPTION'),
            scope: "world",
            config: true,
            default: true,
            type: Boolean,
            onChange: value => OD6S.showSkillSpecialization = value
        })
    })
}

