import OD6S from "../config/config-od6s.js";

export class SpecializeDialog extends Dialog {

    constructor(newItemData, specializeTemplate, data, options) {
        super(data, options);
        this.newItemData = newItemData;
        this.specializeTemplate = specializeTemplate;
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.find('.freeadvancecheckbox').change( async () => {
            /* Whenever this is toggled, reset cpcost */
            this.newItemData.freeadvance = !(this.newItemData.freeadvance);
            if (this.newItemData.freeadvance) {
                this.newItemData.cpcost = 0;
            } else {
                this.newItemData.cpcost = this.newItemData.originalcpcost;
            }
            await this.updateDialog();
        });
        
        html.find('.specializationname').change(async ev => {
            this.newItemData.specname = ev.target.value;
            await this.updateDialog();
        })
    }

    async updateDialog() {
        this.data.content = await renderTemplate(this.specializeTemplate, this.newItemData);
        this.render();
    }
}

export class od6sspecialize {

    activateListeners(html)
    {
        super.activateListeners(html);
    }

    async _onSpecialize(event) {
        event.preventDefault();
        // Create the specialization item, tied to the correct attribute/skill
        const skill = this.actor.getEmbeddedDocument("Item",
            event.currentTarget.dataset.itemId, true);
        const derivedScore = (+skill.data.data.score) + (+this.actor.data.data.attributes[skill.data.data.attribute.toLowerCase()].score) + 1
        const cpCost = Math.floor(Math.floor(derivedScore/OD6S.pipsPerDice)/2);
        let newItemData = {
            specname: "",
            type: "specialization",
            skill: skill.name,
            attribute: skill.data.data.attribute,
            description: skill.data.data.description,
            score: derivedScore,
            cpcost: cpCost,
            originalcpcost: cpCost,
            actor: this.actor
        }
        const specializeTemplate = "systems/od6s/templates/actor/common/specialize.html";
        const html = await renderTemplate(specializeTemplate, newItemData);
        new SpecializeDialog(newItemData, specializeTemplate, {
            title: game.i18n.localize("OD6S.CREATE_SPECIALIZATION") + "!",
            content: html,
            buttons: {
                submit: {
                    label: game.i18n.localize("OD6S.CREATE_SPECIALIZATION"),
                    callback: dlg => od6sspecialize.addSpecialization(
                        this,
                        newItemData,
                        skill.id
                       )
                }
            }
        }).render(true);

    }

    static async addSpecialization(actorSheet, itemData, skillId) {
        // Can't have a blank name
        if (!itemData.specname || itemData.specname==="" ) {
            ui.notifications.error(game.i18n.localize("OD6S.ERR_SPECIALIZATION_NAME"));
            return;
        }

        // Can't spend what you don't have
        if (actorSheet.actor.type === "character") {
            if ((+itemData.cpcost) > (+actorSheet.actor.data.data.characterpoints.value) &&
                actorSheet.actor.data.data.sheetmode !== "freeadvance") {
                ui.notifications.warn(game.i18n.localize("OD6S.NOT_ENOUGH_CP_SPEC"));
                return;
            }
        }

        // Create new specialization item, derived from original skill
        let newItemData = duplicate(actorSheet.actor.getEmbeddedDocument("Item",
            skillId, true));
        const base = (+newItemData.data.base) + 1

        newItemData = {
            name: itemData.specname,
            type: itemData.type,
            data: {
                attribute: itemData.attribute,
                description: itemData.specname,
                base: base,
                time: itemData.time,
                skill: itemData.skill
            }
        }
        await actorSheet.actor.createEmbeddedDocuments('Item', [newItemData]);

        // Deduct character points
        if (actorSheet.actor.type === "character") {
            if ((+itemData.cpcost) > 0 && actorSheet.actor.getFlag('od6s', 'sheetmode' !== "freeadvance")) {
                const update = {};
                update.id = actorSheet.actor.id;
                update.data.data.characterpoints.value -= actorSheet.actor.data.data.characterpoints.value;
                await actorSheet.actor.update(update, {diff: true});
            }
        }

        await actorSheet.render();
    }
}
