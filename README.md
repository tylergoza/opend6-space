# OpenD6 Space System

This is a system intended for playing OpenD6 Space on Foundry VTT.
It uses the rules from the OpenD6 Project SRD found at http://opend6project.org.
Content comes from the OpenD6 Space System, of which a PDF rulebook can be found at https://ogc.rpglibrary.org/index.php?title=OpenD6.
The ruleset has been released under the terms of the OGL v1.0a.

## Installation

In Foundry VTT use the following Manifest URL under "Install System":

https://gitlab.com/api/v4/projects/23950825/packages/generic/od6s/0.0.0/system.json

'0.0.0' will always refer to the latest version.
If you wish to install a specific version you can substitute it for '0.0.0'.

### Compatibility

See the wiki: https://gitlab.com/vtt2/opend6-space/-/wikis/home#basics

### Requirements

* socketlib (https://foundryvtt.com/packages/socketlib)
  * When updating from versions older than 0.1.0 you will want to double-check that socketlib was installed and activated.

## Basic Usage

See the wiki: https://gitlab.com/vtt2/opend6-space/-/wikis/Home

## Customization
There is a level of customization available to tailor the system to your specific universe.
Check the "Settings->Configure Settings->System Settings" menu to see the available options.

## Localization

During creation every attempt was made to ensure that all displayed text uses Foundry VTT's native localization functionality.
If you find any text which does *not* follow this principle, please let me know with an issue.

## Development Progress
I am attempting to keep Gitlab Issues up to date with development tasks tied to milestones.  If you find a bug, feel free to create an issue.

## Credits
This system was started using the Boilerplate System (https://gitlab.com/asacolips-projects/foundry-mods/boilerplate), created by asacolips, as the base.

Pack building from yaml templates using gulp from https://pumbers.github.io/game-manglement/articles/vtt_compendia_building/ 

Lots of love from the Foundry VTT and League of Extraordinary Developers Discord channels.

Configuration menus cribbed heavily from WFRP house rules settings (MooMan!) (https://foundryvtt.com/packages/wfrp4e).

https://www.foundryvtt-hub.com/

Some icons used from http://game-icons.net under https://creativecommons.org/licenses/by/3.0/

Contributers:

French translation provided by @dryasredrock

Spanish translation provided by Skorbuto McFly

@diwako1  
