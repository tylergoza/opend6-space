export default class od6sAutomationConfiguration extends FormApplication {
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "custom_labels";
        options.template = "systems/od6s/templates/settings/settings.html";
        options.width = 600;
        options.minimizable = true;
        options.resizable = true;
        options.title = game.i18n.localize("OD6S.CONFIG_AUTOMATION_OPTIONS_MENU");
        return options;
    }

    getData() {
        let data = super.getData;

        data.settings = Array.from(game.settings.settings).filter(s => s[1].od6sAutomation).map(i => i[1])
        data.settings.forEach(s => s.inputType = s.type == Boolean ? "checkbox" : "text")
        data.settings.forEach(s => s.choice = typeof(s.choices) === 'undefined'  ? false : true)
        data.settings.forEach(s => s.value = game.settings.get(s.namespace, s.key))
        return data;
    }

    async _updateObject(event, formData) {
        for(let setting in formData) {
            await game.settings.set("od6s", setting, formData[setting]);
        }
    }
}