import OD6S from "../config/config-od6s.js";

export class od6sutilities {

    /**
     * Function which returns a number of dice and pips from a raw score.
     * e.g. a score of 14 translates to "4D+2", a score of 15 is "5D+0".
     * @param score
     * @returns {{dice: number, pips: number}}
     */
    static getDiceFromScore(score) {
        const dice = Math.floor(score / OD6S.pipsPerDice);
        const pips = score % OD6S.pipsPerDice;
        return {
            dice,
            pips
        }
    }

    /**
     * Get a score from a number of dice and pips.
     * @param dice
     * @param pips
     * @returns {number}
     */
    static getScoreFromDice(dice, pips) {
        return (+dice * OD6S.pipsPerDice) + (+pips);
    }

    /**
     * Get the action penalty from the actor's wound level vs. the system wound levels
     * @param actor
     * @returns {number}
     */
    static getWoundPenalty(actor) {
        if(OD6S.woundConfig === 1) {
            if (actor.type === 'character') {
                return OD6S.deadliness[3][actor.data.data.wounds.value].penalty;
            } else if (actor.type === 'npc') {
                return OD6S.deadliness[3][actor.data.data.wounds.value].penalty;
            } else if (actor.type === 'creature') {
                return OD6S.deadliness[3][actor.data.data.wounds.value].penalty;
            } else if (actor.type === 'vehicle') {
                return 0;
            } else if (actor.type === 'starship') {
                return 0;
            }
        } else {
            if (actor.type === 'character') {
                return OD6S.deadliness[game.settings.get('od6s', 'deadliness')][actor.data.data.wounds.value].penalty;
            } else if (actor.type === 'npc') {
                return OD6S.deadliness[game.settings.get('od6s', 'npc-deadliness')][actor.data.data.wounds.value].penalty;
            } else if (actor.type === 'creature') {
                return OD6S.deadliness[game.settings.get('od6s', 'creature-deadliness')][actor.data.data.wounds.value].penalty;
            } else if (actor.type === 'vehicle') {
                return 0;
            } else if (actor.type === 'starship') {
                return 0;
            }
        }

    }

    /**
     * Search for and get an item from compendia by name
     * @param itemName
     * @returns {Promise<Entity|null>}
     * @private
     */
    static async _getItemFromCompendium(itemName) {
        let itemList = '';
        let packs = '';
        game.packs.keys();
        if (game.settings.get('od6s', 'hide_compendia')) {
            packs = await game.packs.filter(p => p.metadata.system !== 'od6s')
        } else {
            packs = await game.packs;
        }
        for (let p of packs) {
            await p.getIndex().then(index => itemList = index);
            let searchResult = itemList.find(t => t.name === itemName);
            if (searchResult) {
                return await p.getDocument(searchResult._id);
            }
        }
        return null;
    }

    /**
     * Get an item from the world
     * @param itemName
     * @returns {Promise<*>}
     * @private
     */
    static async _getItemFromWorld(itemName) {
        return game.items.contents.find(t => t.name === itemName);
    }

    /**
     * Get all items of a certain type from compendia
     * @param itemType
     * @returns {Promise<[]>}
     */
    static getItemsFromCompendiumByType(itemType) {
        let searchResult = [];
        let packs = '';
        game.packs.keys();
        if (game.settings.get('od6s', 'hide_compendia')) {
            packs = game.packs.filter(p => p.metadata.system !== 'od6s' && p.documentName === 'Item')
        } else {
            packs = game.packs.filter(p => p.documentName === 'Item');
        }

        for (let p of packs) {
            const items = p.index.filter(i => i.type === itemType);
            searchResult = searchResult.concat(items);
        }

        searchResult.sort((a, b) => a.name.localeCompare(b.name));
        return searchResult;
    }

    /**
     * Get all items of a certain type from the world
     * @param itemType
     * @returns {Promise<[]>}
     */
    static getItemsFromWorldByType(itemType) {
        let searchResult = [];
        for (let i = 0; i < game.items.contents.length; i++) {
            if (game.items.contents[i].type === itemType) {
                let item = {
                    _id: game.items.contents[i]._id,
                    name: game.items.contents[i].data.name,
                    type: game.items.contents[i].type,
                    description: game.items.contents[i].data.data.description
                }
                searchResult.push(item);
            }
        }
        return searchResult;
    }

    /**
     * Get an actor document from a UUID
     * @param uuid
     * @returns {Promise<void>}
     */
    static async getActorFromUuid(uuid) {
        const document = await fromUuid(uuid);
        let actor;
        if (document.documentName === 'Actor') {
            actor = game.actors.get(document.id);
        } else if (document.documentName === 'Token') {
            actor = game.scenes.active.data.tokens.filter(t => t.id === document.id)[0].actor;
        }

        return actor;
    }

    /**
     * Search for a spec, skill, or attribute and return the score
     * @param actor
     * @param spec
     * @param skill
     * @param attribute
     * @returns {*|number}
     */
    static getScoreFromSkill(actor, spec, skill, attribute) {
        let score = 0;
        let found = false;
        // Look for a spec, then a skill, then finally attribute
        if (typeof (spec) !== "undefined" && spec !== '') {
            const foundSpec = actor.items.find(s => s.name === spec && s.type === 'specialization');
            if (foundSpec) {
                score = foundSpec.data.data.score;
                found = true;
            }
        }
        if (!found && typeof (skill) !== "undefined" && skill !== '') {
            const foundSkill = actor.items.find(s => s.name === skill && s.type === 'skill');
            if (foundSkill) {
                score = foundSkill.data.data.score;
            }
        }
        score += actor.data.data.attributes[attribute].score;
        return score;
    }

    /**
     * Return the total sensor score based on skill
     * @param actor
     * @param score
     * @returns {*}
     */
    static getSensorTotal(actor, score) {
        let skillName = '';
        if (actor.getFlag('od6s','crew')) {
            if (typeof(actor.data.data.vehicle.sensors.skill) !== 'undefined'
                && actor.data.data.vehicle.sensors.skill !== '') {
                skillName = actor.data.data.vehicle.sensors.skill
            }
        }
        if (skillName == '') {
            skillName = game.i18n.localize(OD6S.default_sensor_skill);
        }
        return (+score) + od6sutilities.getScoreFromSkill(actor, '', skillName, 'mec');
    }

    static async autoOpposeRoll(msg) {
        if (game.settings.get('od6s', 'use_wild_die')
            && msg.getFlag('od6s', 'wild') && !msg.getFlag('od6s','wildHandled')) return;
        const token = game.scenes.active.data.tokens.get(msg.getFlag('od6s', 'targetId'))
        if (typeof (token) !== 'undefined') {
            await this.generateOpposedRoll(token, msg);
        }
        if (OD6S.opposed.length > 0) {
            if (msg.getFlag('od6s', 'type') === 'damage') {
                // Shouldn't be here, damage needs to come before resistance.
                OD6S.opposed = [];
                OD6S.opposed.push(msg.id);
            } else if (msg.getFlag('od6s', 'type') === 'resistance') {
                OD6S.opposed.push(msg.id);
                return await this.handleOpposedRoll();
            }
        } else {
            if (msg.getFlag('od6s', 'type') === 'damage') {
                OD6S.opposed.push(msg.id);
            } else {
                OD6S.opposed = [];
            }
        }
    }

    static async handleOpposedRoll() {
        let type = '';
        let winner = '';
        let loser = '';
        let diff = 0;
        let result = '';
        let damageFlavor = '';
        let data = {};
        data.flags = {};
        let collision = false;
        let passengerDamage = '';
        const message1 = game.messages.get(OD6S.opposed[0]);
        const message2 = game.messages.get(OD6S.opposed[1]);
        OD6S.opposed = [];

        if ((message1.getFlag('od6s', 'type') === 'damage' && message2.getFlag('od6s', 'type') === 'resistance') ||
            (message1.getFlag('od6s', 'type') === 'resistance' && message2.getFlag('od6s', 'type') === 'damage')) {
            type = "damageresult";
        } else {
            type = "opposedcheck";
        }

        collision = (message1.getFlag('od6s', 'isVehicleCollision') || message2.getFlag('od6s', 'isVehicleCollision'))
        collision = (collision === 'true');

        if (typeof (game.actors.get(message1.data.speaker.actor)) !== "undefined") {
            message1.actorType = game.actors.get(message1.data.speaker.actor).type;
        } else {
            message1.actorType = "system";
        }

        if (typeof (game.actors.get(message2.data.speaker.actor)) !== "undefined") {
            message1.actorType = game.actors.get(message2.data.speaker.actor).type;
        } else {
            message2.actorType = "system";
        }

        message1.flavorName = message1.alias;
        message2.flavorName = message2.alias;

        if (message1.getFlag('od6s', 'vehicle')) {
            message1.actorType = "vehicle";
            let vehicleActor = await od6sutilities.getActorFromUuid(message1.getFlag('od6s', 'vehicle'));
            message1.flavorName = vehicleActor.name;
        }
        if (message2.getFlag('od6s', 'vehicle')) {
            message2.actorType = "vehicle";
            let vehicleActor = await od6sutilities.getActorFromUuid(message2.getFlag('od6s', 'vehicle'));
            message2.flavorName = vehicleActor.name;
        }


        if (message1.roll.total > message2.roll.total) {
            winner = message1;
            loser = message2;
        } else {
            winner = message2;
            loser = message1;
        }

        diff = (+winner.roll.total) - (+loser.roll.total);

        if (type === "damageresult") {

            if (loser.actorType === "vehicle" || loser.actorType === "starship") {
                damageFlavor = game.i18n.localize('OD6S.DAMAGES');
            } else {
                damageFlavor = game.i18n.localize("OD6S.INJURES")
            }

            if (winner.getFlag('od6s', 'type') === "damage") {
                data.content = winner.alias + " " + damageFlavor + " " + loser.flavorName;
                if (OD6S.woundConfig > 0 && loser.actorType !== 'vehicle' && loser.actorType !== 'starship') {
                    result = diff;
                } else {
                    result = this.getInjury(diff, loser.actorType);
                }
            } else {
                data.content = winner.alias + " " + game.i18n.localize("OD6S.RESISTS") + " " + loser.alias;
                if (winner.actorType === "vehicle" || winner.actorType === "starship") {
                    result = 'OD6S.NO_DAMAGE';
                } else {
                    if (OD6S.woundConfig > 0 && loser.actorType !== 'vehicle' && loser.actorType !== 'starship') {
                        result = 0;
                    } else {
                        result ='OD6S.NO_INJURY';
                    }
                }
            }
        } else {
            data.flavor = message1.alias + " " + game.i18n.localize("OD6S.VS") + " " + message2.alias;
            data.content = winner.alias + " " + game.i18n.localize("OD6S.WINS");
        }

        if (loser.actorType === "vehicle" || loser.actorType === "starship") {
            if (OD6S.passengerDamageDice) {
                passengerDamage = OD6S.vehicle_damage[result].passenger_damage_dice + "D";
            } else {
                passengerDamage = game.i18n.localize(OD6S.vehicle_damage[result].passenger_damage);
            }
        }

        let apply = false;
        if (OD6S.woundConfig > 0 && loser.actorType !== 'vehicle' && loser.actorType !== 'starship') {
            if (result > 0) apply = true;
        } else if (result !== 'OD6S.NO_INJURY' && result !== 'OD6S.NO_DAMAGE') {
            apply = true;
        }

        data.flags.od6s = {
            "isOpposed": true,
            "type": type,
            "isVisible": false,
            "result": result,
            "apply": apply,
            "applied": false,
            "loserIsVehicle": loser.actorType === 'vehicle' || loser.actorType === 'starship',
            "loserId": loser.data.speaker.token,
            "isCollision": collision,
            "passengerDamage": passengerDamage
        }
        await ChatMessage.create(data);
    }

    static async generateOpposedRoll(token, msg) {
        if (token.actor.type === 'npc' || token.actor.type === 'creature') {
            if (msg.getFlag('od6s', 'type') === 'damage') {
                const type = msg.getFlag('od6s', 'damageType');
                if (type === 'e') {
                    return await token.actor.rollAction('er');
                } else if (type === 'p') {
                    return await token.actor.rollAction('pr');
                }
            }
        }
    }

    static getInjury(damage, actorType) {
        let resultMessage = '';
        if (actorType === "vehicle" || actorType === "starship") {
            for (let result in OD6S.vehicle_damage) {
                if (damage >= OD6S.vehicle_damage[result].damage) {
                    resultMessage = result;
                } else {
                    break;
                }
            }
        } else {
            for (let result in OD6S.damage) {
                if (damage >= OD6S.damage[result]) {
                    resultMessage = result;
                } else {
                    break;
                }
            }
        }
        return resultMessage;
    }

    static waitFor3DDiceMessage(targetMessageId) {
        function buildHook(resolve) {
            Hooks.once('diceSoNiceRollComplete', (messageId) => {
                if (targetMessageId === messageId)
                    resolve(true);
                else
                    buildHook(resolve)
            });
        }
        return new Promise((resolve,reject) => {
            if(game.dice3d){
                buildHook(resolve);
            } else {
                resolve(true);
            }
        });
    }


}


