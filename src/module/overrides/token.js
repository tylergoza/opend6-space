export class OD6SToken extends Token {

    _canDrag(user, event) {
        if (!this._controlled) return false;
        if (!user.isGM && event.data.object.actor.type === 'container') return false;
        const tool = game.activeTool;
        if ((tool !== "select") || game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.CONTROL)) return false;
        const blockMove = game.paused && !game.user.isGM;
        return !this._movement && !blockMove;
    }
}


