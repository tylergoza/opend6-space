import {od6sutilities} from "../system/utilities.js";
import OD6S from "../config/config-od6s.js";

export class AdvanceDialog extends Dialog {

    constructor(actorSheet, advanceData, advanceTemplate, data, options) {
        super(data, options);
        this.actorSheet = actorSheet;
        this.advanceData = advanceData;
        this.advanceTemplate = advanceTemplate;
    }

    activateListeners(html) {

        super.activateListeners(html);

        html.find('.freeadvancecheckbox').change( async () => {
            /* Whenever this is toggled, reset values */
            this.advanceData.cpcost = 0;
            this.advanceData.score = this.advanceData.originalscore;
            this.advanceData.freeadvance = !(this.advanceData.freeadvance);
            await this.updateDialog();
        });

        html.find('.advanceup').click(async () => {
            this.pipUp();
            await this.updateDialog();
        });

        html.find('.advancedown').click(async () => {
            this.pipDown()
            await this.updateDialog();
        });
    }

    async updateDialog() {
        this.advanceData.cpcost > this.actorSheet.actor.data.data.characterpoints.value ? this.advanceData.cpcostcolor="red" :
            this.advanceData.cpcostcolor="black";
        const advanceTemplate = this.advanceTemplate;
        this.data.content = await renderTemplate(advanceTemplate, this.advanceData);
        this.render();
    }

    cpCost(up) {
        if (this.advanceData.freeadvance) {
            return
        }

        let score;
        OD6S.flatSkills ? score = this.advanceData.base :
            score = od6sutilities.getDiceFromScore(this.advanceData.score);

        if (up) {
            // First advance in metaphysics costs 20cp
            if ( (this.advanceData.type === "attribute")
                &&  (this.advanceData.label === game.i18n.localize("OD6S.CHAR_METAPHYSICS"))
                &&  (this.advanceData.score === 0)) {

                // First meta advance costs 20cp
                this.advanceData.cpcost = 20;
                return
            }

            if (this.advanceData.type === "attribute") {
                this.advanceData.cpcost += ((+score.dice) * 10);
            } else if (this.advanceData.type === "skill") {
                OD6S.flatSkills ? this.advanceData.cpcost += (+this.advanceData.base) + 1 :
                    this.advanceData.cpcost += (+score.dice);
            } else if (this.advanceData.type === "specialization") {
                OD6S.flatSkills ? this.advanceData.cpcost += Math.ceil(((+this.advanceData.base) + 1)/2) :
                this.advanceData.cpcost += Math.ceil(+score.dice/2)
            }
        } else {

            if ( (this.advanceData.type === "attribute")
                &&  (this.advanceData.label === game.i18n.localize("OD6S.CHAR_METAPHYSICS"))
                &&  (this.advanceData.score === OD6S.pipsPerDice)) {

                // First meta advance costs 20cp
                this.advanceData.cpcost = 0;
                return
            }

            if (this.advanceData.cpcost <= 0) {
                this.advanceData.cpcost = 0;
                return;
            }

            if (score.pips === 0) {
                -- score.dice;
            }
            if (this.advanceData.type === "attribute") {
                this.advanceData.cpcost -= ((+score.dice) * 10);
            } else if (this.advanceData.type === "skill") {
                OD6S.flatSkills ? this.advanceData.cpcost -= (+this.advanceData.base) :
                    this.advanceData.cpcost -= (+score.dice);
            } else if (this.advanceData.type === "specialization") {
                OD6S.flatSkills ? this.advanceData.cpcost -= Math.ceil(((+this.advanceData.base))/2) :
                    this.advanceData.cpcost -= Math.ceil(+score.dice/2)
            }
        }
    }

    pipUp() {
        // First meta advance goes straight to 1D
        if ( (this.advanceData.type === "attribute")
            &&  (this.advanceData.label === game.i18n.localize("OD6S.CHAR_METAPHYSICS"))
            &&  (this.advanceData.score === 0)) {

            this.cpCost(true);
            this.advanceData.score = OD6S.pipsPerDice;
            return(true);
        }

        /* Only allow one advance per dialog per attribute/item */
        if ((this.advanceData.originalscore < this.advanceData.score)
                && (!this.advanceData.freeadvance)) {
            ui.notifications.warn(game.i18n.localize("OD6S.ALREADY_ADVANCED") );
            return(false);
        }

        this.cpCost(true);
        if(OD6S.flatSkills) this.advanceData.base ++;
        this.advanceData.score ++;
        return(true);
    }

    pipDown() {
        // First meta advance goes straight to 1D
        if ( (this.advanceData.type === "attribute")
            &&  (this.advanceData.label === game.i18n.localize("OD6S.CHAR_METAPHYSICS"))
            &&  (this.advanceData.score === OD6S.pipsPerDice)) {

            this.cpCost(false);
            this.advanceData.score = 0;
            return(true);
        }

        /* Decrement pips down by 2->1->0, then take a die, keep track of original score */
        if (this.advanceData.score < 1) {
            // Can't go below zero
            return(false);
        }

        if(this.advanceData.score <= this.advanceData.originalscore) {
            // Can't go below the original score
            return(false);
        }

        this.cpCost(false);
        if(OD6S.flatSkills) this.advanceData.base --;
        this.advanceData.score --;
        return(true);
    }
}

export class od6sadvance {

    activateListeners(html)
    {
        super.activateListeners(html);
    }

    async _onAdvance(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const dataset = element.dataset;
        let originalScore = 0;
        let cpcost = 0;
        let dice = dataset.dice;
        let pips = dataset.pips;
        let base = dataset.base;
        let freeAdvance = Boolean(false);
        let itemid = 0;
        const actorData = this.actor.data.data

        /* Determine the type of thing we're trying to advance so we can set the correct data fields */
        if (dataset.type === "skill") {
            const skill = this.actor.getEmbeddedDocument("Item", dataset.itemId);
            let attribute;
            for (attribute in this.actor.data.data.attributes) {
                if (skill.data.data.attribute === attribute) {
                    originalScore = (+skill.data.data.base);
                    if (!OD6S.flatSkills)
                        originalScore += (+actorData.attributes[attribute].base)
                }
            }
            itemid = dataset.itemId;
        } else if (dataset.type === "attribute") {
            const attrname = dataset.attrname;
            originalScore = actorData.attributes[attrname].base;
        } else if (dataset.type === "specialization") {
            const spec = this.actor.getEmbeddedDocument("Item", dataset.itemId);
            const skill = this.actor.getEmbeddedDocument("Item", spec.data.skill);
            let attribute;
            for (attribute in this.actor.data.data.attributes) {
                if (spec.data.data.attribute === attribute) {
                    originalScore = (+spec.data.data.base);
                    if (!OD6S.flatSkills)
                        originalScore += (+actorData.attributes[attribute].base)
                }
            }
            itemid = dataset.itemId;
        }

        /* Structure to pass to dialog */
        let advanceData = {
            label: dataset.label,
            score: originalScore,
            base: base,
            cpcost: cpcost,
            cpcostcolor: "black",
            freeadvance: freeAdvance,
            type: dataset.type,
            originalscore: originalScore,
            itemid: itemid
        }

        const advanceTemplate = "systems/od6s/templates/actor/character/advance.html";
        const html = await renderTemplate(advanceTemplate, advanceData);

        let d;
        if(OD6S.flatSkills) {
            d = new AdvanceDialog(this, advanceData, advanceTemplate, {
                title: game.i18n.localize("OD6S.ADVANCE") + "!",
                content: html,
                buttons: {
                    advance: {
                        label: game.i18n.localize("OD6S.ADVANCE"),
                        callback: dlg => od6sadvance.advanceAction(
                            d.actorSheet.actor,
                            d.advanceData,
                            event,
                            $(dlg[0]).find("#base")[0].base)
                    }
                }
            }).render(true);
        } else {
            d = new AdvanceDialog(this, advanceData, advanceTemplate, {
                title: game.i18n.localize("OD6S.ADVANCE") + "!",
                content: html,
                buttons: {
                    advance: {
                        label: game.i18n.localize("OD6S.ADVANCE"),
                        callback: dlg => od6sadvance.advanceAction(
                            d.actorSheet.actor,
                            d.advanceData,
                            event,
                            $(dlg[0]).find("#dice")[0].value,
                            $(dlg[0]).find("#pips")[0].value)
                    }
                }
            }).render(true);
        }
    }

    static async advanceAction(actor, advanceData, event, dice, pips) {

        const actorData = actor.data.data;
        let update = '';
        const actorUpdate = {};
        actorUpdate.data = {};

        /* freeadvance was checked, use form data instead */
        if (advanceData.freeadvance) {
            OD6S.flatSkills ? advanceData.score = advanceData.base :
                advanceData.score = od6sutilities.getScoreFromDice(dice, pips);
        }

        /* Character Point cost is too high. */
        if (!advanceData.freeadvance) {
            if (advanceData.cpcost > actorData.characterpoints.value) {
                ui.notifications.warn(game.i18n.localize("OD6S.NOT_ENOUGH_CP_ADVANCE"));
                return;
            }
        }

        /* Determine item or attribute */
        if (event.currentTarget.dataset.type === "attribute") {
            actorUpdate.data.attributes = {};
            actorUpdate.data.attributes[event.currentTarget.dataset.attrname] = {};
            actorUpdate.data.attributes[event.currentTarget.dataset.attrname].base = advanceData.score;
        }

        if(event.currentTarget.dataset.type === "skill") {
            const skills = actor.data.items.filter(i => i.type === "skill");
            /* Add/subtract to item score, not displayed/aggregate score */
            let newScore;
            OD6S.flatSkills ? newScore = advanceData.base : newScore = advanceData.score - advanceData.originalscore;
            if (!OD6S.flatSkills) {
                newScore = (+newScore) +
                    (+actor.getEmbeddedDocument("Item", advanceData.itemid, true).data.data.base);
            }
            update = skills.map( () => {
                return {
                    _id: advanceData.itemid,
                    "data.base": newScore,
                }
            })
        }

        if(event.currentTarget.dataset.type === "specialization") {
            const specs = actor.data.items.filter(i => i.type === "specialization");
            /* Add/subtract to item score, not displayed/aggregate score */
            let newScore;
            OD6S.flatSkills ? newScore = advanceData.base : newScore = advanceData.score - advanceData.originalscore;
            if (!OD6S.flatSkills) {
                newScore = (+newScore) +
                    (+actor.getEmbeddedDocument("Item", advanceData.itemid, true).data.data.base);
            }
            update = specs.map( () => {
                return {
                    _id: advanceData.itemid,
                    "data.base": newScore,
                }
            })
        }

        if (advanceData.cpcost > 0) {
            actorUpdate.data.characterpoints = {};
            actorUpdate.data.characterpoints.value = actorData.characterpoints.value -= (+advanceData.cpcost);
            if (actorUpdate.data.characterpoints.value < 0) {
                actorUpdate.data.characterpoints.value = 0;
            }
        }

        actorUpdate.id = actor.id;

        await actor.update(actorUpdate, {diff: true});
        if(update !== "") {await actor.updateEmbeddedDocuments("Item", update)}
        await actor.render();
    }
}
