const OD6S = {};

OD6S.startCombat = false;
OD6S.socket = '';
OD6S.baseHitDifficulty = 10;
OD6S.default_sensor_skill = "OD6S.SENSORS";
OD6S.fatePointsName = '';
OD6S.fatePointsShortName = '';
OD6S.useAFatePointName = '';
OD6S.metaphysicsName = '';
OD6S.manifestationsName = '';
OD6S.metaphysicsExtranormalName = '';
OD6S.vehicleToughnessName = '';
OD6S.passengerDamageDice = false;
OD6S.starshipToughessName = '';
OD6S.vehicleDifficulty = true;
OD6S.brawlAttribute = '';
OD6S.opposed = [];
OD6S.chatPath = 'systems/od6s/templates/chat/';
OD6S.wildDieOneDefault = 0;
OD6S.wildDieOneAuto = 0;
OD6S.grenadeDamageDice = false;
OD6S.highlightEffects = false;
OD6S.randomHitLocations = false;
OD6S.mapRange = false;
OD6S.meleeDifficulty = false;
OD6S.baseAttackDifficulty = 10;
OD6S.defenseLock = false;
OD6S.currencyName = "OD6S.CHAR_CREDITS";
OD6S.fatePointRound = false;
OD6S.fatePointClimactic = false;
OD6S.woundConfig = 0;
OD6S.bodyPointsName = "OD6S.BODY_POINTS";
OD6S.highHitDamage = false;
OD6S.autoOpposed = false;
OD6S.pipsPerDice = 3;
OD6S.speciesMaxDice = 1;
OD6S.speciesMinDice = 5;
OD6S.flatSkills = false;
OD6S.cost = 0;
OD6S.fundsFate = false;
OD6S.showSkillSpecialization = true;
OD6S.channelSkillName = "OD6S.METAPHYSICS_SKILL_CHANNEL";
OD6S.senseSkillName = "OD6S.METAPHYSICS_SKILL_SENSE";
OD6S.transformSkillName = "OD6S.METAPHYSICS_SKILL_TRANSFORM";

OD6S.deadlinessLevel = {
    "character": 3,
    "creature": 3,
    "npc": 3
}

OD6S.wildDieResult = {
    0: "OD6S.WILD_RESULT_ONE",
    1: "OD6S.WILD_DIE_NONE",
    2: "OD6S.REMOVE_HIGHEST_DIE",
    3: "OD6S.COMPLICATION"
}

OD6S.actorMasks = {
    "character": 0,
    "npc": 1,
    "creature": 2,
    "vehicle": 3,
    "starship": 4
}

OD6S.equippable = [
    "weapon",
    "armor",
    "gear",
    "vehicle-weapon",
    "vehicle-gear",
    "starship-weapon",
    "starship-gear"
]

OD6S.cargo_hold = [
    "weapon",
    "armor",
    "gear",
    "vehicle-weapon",
    "vehicle-gear",
    "starship-weapon",
    "starship-gear"
]

OD6S.deadliness = {
    0: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
            "core": "OD6S.WOUNDS_HEALTHY"
        },
        1: {
            "description": "OD6S.WOUNDS_STUNNED",
            "penalty": 0,
            "core": "OD6S.WOUNDS_STUNNED"
        },
        2: {
            "description": "OD6S.WOUNDS_WOUNDED_1",
            "penalty": 0,
            "core": "OD6S.WOUNDS_WOUNDED"
        },
        3: {
            "description": "OD6S.WOUNDS_WOUNDED_2",
            "penalty": 1,
            "core": "OD6S.WOUNDS_WOUNDED"
        },
        4: {
            "description": "OD6S.WOUNDS_WOUNDED_3",
            "penalty": 1,
            "core": "OD6S.WOUNDS_WOUNDED"
        },
        5: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
            "core": "OD6S.WOUNDS_SEVERELY_WOUNDED"
        },
        6: {
            "description": "OD6S.WOUNDS_INCAPACITATED",
            "penalty": 3,
            "core": "OD6S.WOUNDS_INCAPACITATED"
        },
        7: {
            "description": "OD6S.WOUNDS_MORTALLY_WOUNDED",
            "penalty": 0,
            "core": "OD6S.WOUNDS_MORTALLY_WOUNDED"
        },
        8: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
            "core": "OD6S.WOUNDS_DEAD"
        },
    },
    2: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
            "core": "OD6S.WOUNDS_HEALTHY"
        },
        1: {
            "description": "OD6S.WOUNDS_STUNNED",
            "penalty": 0,
            "core": "OD6S.WOUNDS_STUNNED"
        },
        2: {
            "description": "OD6S.WOUNDS_WOUNDED_1",
            "penalty": 1,
            "core": "OD6S.WOUNDS_WOUNDED"
        },
        3: {
            "description": "OD6S.WOUNDS_WOUNDED_2",
            "penalty": 1,
            "core": "OD6S.WOUNDS_WOUNDED"
        },
        4: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
            "core": "OD6S.WOUNDS_SEVERELY_WOUNDED"
        },
        5: {
            "description": "OD6S.WOUNDS_INCAPACITATED",
            "penalty": 3,
            "core": "OD6S.WOUNDS_INCAPACITATED"
        },
        6: {
            "description": "OD6S.WOUNDS_MORTALLY_WOUNDED",
            "penalty": 0,
            "core": "OD6S.WOUNDS_MORTALLY_WOUNDED"
        },
        7: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
            "core": "OD6S.WOUNDS_DEAD"
        },
    },
    3: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
            "core": "OD6S.WOUNDS_HEALTHY"
        },
        1: {
            "description": "OD6S.WOUNDS_STUNNED",
            "penalty": 0,
            "core": "OD6S.WOUNDS_STUNNED"
        },
        2: {
            "description": "OD6S.WOUNDS_WOUNDED",
            "penalty": 1,
            "core": "OD6S.WOUNDS_WOUNDED"
        },
        3: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
            "core": "OD6S.WOUNDS_SEVERELY_WOUNDED"
        },
        4: {
            "description": "OD6S.WOUNDS_INCAPACITATED",
            "penalty": 3,
            "core": "OD6S.WOUNDS_INCAPACITATED"
        },
        5: {
            "description": "OD6S.WOUNDS_MORTALLY_WOUNDED",
            "penalty": 0,
            "core": "OD6S.WOUNDS_MORTALLY_WOUNDED"
        },
        6: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
            "core": "OD6S.WOUNDS_DEAD"
        },
    },
    4: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
            "core": "OD6S.WOUNDS_HEALTHY"
        },
        1: {
            "description": "OD6S.WOUNDS_WOUNDED",
            "penalty": 1,
            "core": "OD6S.WOUNDS_WOUNDED"
        },
        2: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
            "core": "OD6S.WOUNDS_SEVERELY_WOUNDED"
        },
        3: {
            "description": "OD6S.WOUNDS_INCAPACITATED",
            "penalty": 3,
            "core": "OD6S.WOUNDS_INCAPACITATED"
        },
        4: {
            "description": "OD6S.WOUNDS_MORTALLY_WOUNDED",
            "penalty": 0,
            "core": "OD6S.WOUNDS_MORTALLY_WOUNDED"
        },
        5: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
            "core": "OD6S.WOUNDS_DEAD"
        },
    },
    5: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
            "core": "OD6S.WOUNDS_HEALTHY"
        },
        1: {
            "description": "OD6S.WOUNDS_WOUNDED",
            "penalty": 1,
            "core": "OD6S.WOUNDS_WOUNDED"
        },
        2: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
            "core": "OD6S.WOUNDS_SEVERELY_WOUNDED"
        },
        3: {
            "description": "OD6S.WOUNDS_MORTALLY_WOUNDED",
            "penalty": 3,
            "core": "OD6S.WOUNDS_MORTALLY_WOUNDED"
        },
        4: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
            "core": "OD6S.WOUNDS_DEAD"
        },
    }
}

OD6S.damage = {
    "OD6S.WOUNDS_STUNNED": 1,
    "OD6S.WOUNDS_WOUNDED": 4,
    "OD6S.WOUNDS_INCAPACITATED": 9,
    "OD6S.WOUNDS_MORTALLY_WOUNDED": 13,
    "OD6S.WOUNDS_DEAD": 16
}

OD6S.bodyPointLevels = {
    "OD6S.WOUNDS_HEALTHY": 9999,
    "OD6S.WOUNDS_STUNNED": 81,
    "OD6S.WOUNDS_WOUNDED": 60,
    "OD6S.WOUNDS_SEVERELY_WOUNDED": 40,
    "OD6S.WOUNDS_INCAPACITATED": 20,
    "OD6S.WOUNDS_MORTALLY_WOUNDED": 10,
    "OD6S.WOUNDS_DEAD": 1
}

OD6S.vehicle_damage = {
    "OD6S.DAMAGE_NONE": {
        "damage": 0,
        "passenger_damage": "OD6S.PASSENGER_NO_DAMAGE",
        "passenger_damage_dice": 0
    },
    "OD6S.DAMAGE_VERY_LIGHT": {
        "damage": 1,
        "passenger_damage": "OD6S.PASSENGER_NO_DAMAGE",
        "passenger_damage_dice": 0
    },
    "OD6S.DAMAGE_LIGHT": {
        "damage": 4,
        "passenger_damage": "OD6S.PASSENGER_QUARTER_DAMAGE",
        "passenger_damage_dice": 1
    },
    "OD6S.DAMAGE_HEAVY": {
        "damage": 9,
        "passenger_damage": "OD6S.PASSENGER_HALF_DAMAGE",
        "passenger_damage_dice": 3
    },
    "OD6S.DAMAGE_SEVERE": {
        "damage": 13,
        "passenger_damage": "OD6S.PASSENGER_THREE_QUARTERS_DAMAGE",
        "passenger_damage_dice": 6
    },
    "OD6S.DAMAGE_DESTROYED": {
        "damage": 16,
        "passenger_damage": "OD6S.PASSENGER_FULL_DAMAGE",
        "passenger_damage_dice": 12
    }
}

OD6S.vehicle_speeds = {
    "stopped": {
        "name": "OD6S.VEHICLE_SPEED_STOPPED",
        "damage": 6,
        "mod": 0
    },
    "cautious": {
        "name": "OD6S.VEHICLE_SPEED_CAUTIOUS",
        "damage": 12,
        "mod": 0
    },
    "cruise": {
        "name": "OD6S.VEHICLE_SPEED_CRUISE",
        "damage": 18,
        "mod": 0
    },
    "high": {
        "name": "OD6S.VEHICLE_SPEED_HIGH",
        "damage": 24,
        "mod": 5
    },
    "all_out": {
        "name": "OD6S.VEHICLE_SPEED_ALL_OUT",
        "damage": 30,
        "mod": 10
    }
}

OD6S.collision_types = {
    "head_on": {
        "name": "OD6S.VEHICLE_HEAD_ON",
        "score": 9
    },
    "sidewipe": {
        "name": "OD6S.VEHICLE_SIDESWIPE",
        "score": -9
    },
    "rear_end": {
        "name": "OD6S.VEHICLE_REAR_END",
        "score": -9
    },
    "t_bone": {
        "name": "OD6S.VEHICLE_T_BONE",
        "score": 0
    }
}

OD6S.weaponTypes = [
    "OD6S.RANGED",
    "OD6S.MELEE",
    "OD6S.MISSILE",
    "OD6S.THROWN",
    "OD6S.EXPLOSIVE"
]

OD6S.meleeDifficulties = [
    "OD6S.DIFFICULTY_VERY_EASY",
    "OD6S.DIFFICULTY_EASY",
    "OD6S.DIFFICULTY_MODERATE",
    "OD6S.DIFFICULTY_DIFFICULT",
    "OD6S.DIFFICULTY_VERY_DIFFICULT",
    "OD6S.DIFFICULTY_HEROIC",
]

OD6S.actions = {
    "ranged_attack": {
        "name": "OD6S.ACTION_RANGED_ATTACK",
        "type": "rangedattack",
        "rollable": true,
        "base": "agi",
        "skill": "",
        "subtype": "rangedattack"
    },
    "melee_attack": {
        "name": "OD6S.ACTION_MELEE_ATTACK",
        "type": "meleeattack",
        "rollable": true,
        "base": "agi",
        "skill": "Melee Combat",
        "subtype": "meleeattack",

    },
    "brawl_attack": {
        "name": "OD6S.ACTION_BRAWL_ATTACK",
        "type": "brawlattack",
        "rollable": true,
        "base": "agi",
        "skill": "Brawling",
        "subtype": "brawlattack",
    },
    "dodge": {
        "name": "OD6S.ACTION_DODGE",
        "type": "dodge",
        "rollable": true,
        "base": "agi",
        "skill": "Dodge",
        "subtype": "dodge",
    },
    "parry": {
        "name": "OD6S.ACTION_PARRY",
        "type": "parry",
        "rollable": true,
        "base": "agi",
        "skill": "OD6S.ACTION_MELEE_PARRY",
        "subtype": "parry",
    },
    "block": {
        "name": "OD6S.ACTION_BLOCK",
        "type": "block",
        "rollable": true,
        "base": "agi",
        "skill": "OD6S.ACTION_BRAWL_BLOCK",
        "subtype": "block",
    },
    "other": {
        "name": "OD6S.ACTION_OTHER",
        "type": "action",
        "rollable": false,
        "subtype": "misc"
    }
}

OD6S.vehicle_actions = {
    "ranged_attack": {
        "name": "OD6S.ACTION_VEHICLE_RANGED_ATTACK",
        "type": "vehiclerangedattack",
        "rollable": true,
        "base": "mec"
    },
    "ram": {
        "name": "OD6S.ACTION_VEHICLE_RAM",
        "type": "vehicleramattack",
        "rollable": true,
        "base": "mec"
    },
    "dodge": {
        "name": "OD6S.ACTION_VEHICLE_DODGE",
        "type": "vehicledodge",
        "rollable": true,
        "base": "mec"
    },
    "maneuver": {
        "name": "OD6S.ACTION_VEHICLE_MANEUVER",
        "type": "vehiclemaneuver",
        "rollable": true,
        "base": "mec"
    },
    "sensors": {
        "name": "OD6S.ACTION_VEHICLE_SENSORS",
        "type": "vehiclesensors",
        "base": "mec",
        "skill": "OD6S.SENSORS",
        "rollable": true
    },
    "other": {
        "name": "OD6S.ACTION_VEHICLE_OTHER",
        "type": "action",
        "rollable": false
    }
}

OD6S.difficulty = {
    "OD6S.DIFFICULTY_UNKNOWN": {
        "min": 0,
        "max": 0
    },
    "OD6S.DIFFICULTY_CUSTOM": {
        "min": 0,
        "max": 0
    },
    "OD6S.DIFFICULTY_AUTOMATIC": {
        "min": 0,
        "max": 0
    },
    "OD6S.DIFFICULTY_VERY_EASY": {
        "min": 1,
        "max": 5
    },
    "OD6S.DIFFICULTY_EASY": {
        "min": 6,
        "max": 10
    },
    "OD6S.DIFFICULTY_MODERATE": {
        "min": 11,
        "max": 15
    },
    "OD6S.DIFFICULTY_DIFFICULT": {
        "min": 16,
        "max": 20
    },
    "OD6S.DIFFICULTY_VERY_DIFFICULT": {
        "min": 21,
        "max": 25
    },
    "OD6S.DIFFICULTY_HEROIC": {
        "min": 26,
        "max": 30
    },
    "OD6S.DIFFICULTY_LEGENDARY": {
        "min": 31,
        "max": 40
    }
}

OD6S.difficultyShort = {
    "VE": "OD6S.DIFFICULTY_VERY_EASY",
    "E": "OD6S.DIFFICULTY_EASY",
    "M": "OD6S.DIFFICULTY_MODERATE",
    "D": "OD6S.DIFFICULTY_DIFFICULT",
    "VD": "OD6S.DIFFICULTY_VERY_DIFFICULT",
    "H": "OD6S.DIFFICULTY_HEROIC",
    "L": "OD6S.DIFFICULTY_LEGENDARY"
}

OD6S.terrain_difficulty = {
    "OD6S.TERRAIN_EASY": {
        "mod": 0
    },
    "OD6S.TERRAIN_MODERATE": {
        "mod": 5
    },
    "OD6S.TERRAIN_ROUGH": {
        "mod": 10
    },
    "OD6S.TERRAIN_VERY_ROUGH": {
        "mod": 15
    },
    "OD6S.TERRAIN_HAZARDOUS": {
        "mod": 20
    },
    "OD6S.TERRAIN_VERY_HAZARDOUS": {
        "mod": 25
    }
}

OD6S.result = {
    "OD6S.FAILURE": {
        "description": "OD6S.FAILURE",
        "difference": -1
    },
    "OD6S.RESULT_MINIMAL": {
        "description": "OD6S.RESULT_MINIMAL_DESCRIPTION",
        "difference": 0
    },
    "OD6S.RESULT_SOLID": {
        "description": "OD6S.RESULT_SOLID_DESCRIPTION",
        "difference": 1
    },
    "OD6S.RESULT_GOOD": {
        "description": "OD6S.RESULT_GOOD_DESCRIPTION",
        "difference": 5
    },
    "OD6S.RESULT_SUPERIOR": {
        "description": "OD6S.RESULT_SUPERIOR_DESCRIPTION",
        "difference": 9
    },
    "OD6S.RESULT_SPECTACULAR": {
        "description": "OD6S.RESULT_SPECTACULAR_DESCRIPTION",
        "difference": 13
    },
    "OD6S.RESULT_INCREDIBLE": {
        "description": "OD6S.RESULT_INCREDIBLE_DESCRIPTION",
        "difference": 16
    }
}

OD6S.cover = {
    "OD6S.COVER_SMOKE": {
        "OD6S.NONE": {
            "modifier": 0
        },
        "OD6S.COVER_LIGHT_SMOKE": {
            "modifier": 3
        },
        "OD6S.COVER_THICK_SMOKE": {
            "modifier": 6
        },
        "OD6S.COVER_VERY_THICK_SMOKE": {
            "modifier": 12
        },
    },
    "OD6S.COVER_LIGHT": {
        "OD6S.COVER_LIGHT_NONE": {
            "modifier": 0
        },
        "OD6S.COVER_POOR_LIGHT": {
            "modifier": 3
        },
        "OD6S.COVER_MOONLIGHT_NIGHT": {
            "modifier": 6
        },
        "OD6S.COVER_COMPLETE_DARKNESS": {
            "modifier": 12
        },
    },
    "OD6S.COVER": {
        "OD6S.NONE": {
            "modifier": 0
        },
        "OD6S.COVER_QUARTER": {
            "modifier": 3
        },
        "OD6S.COVER_HALF": {
            "modifier": 6
        },
        "OD6S.COVER_THREE_QUARTERS": {
            "modifier": 12
        },
        "OD6S.COVER_FULL": {
            "modifier": 0
        }
    }
}


// Other modifiers besides range and cover
OD6S.calledShot = {
    "OD6S.CALLED_SHOT_NONE": {
        "modifier": 0,
        "damage": 0
    },
    "OD6S.CALLED_SHOT_LARGE": {
        "modifier": 3,
        "damage": 0

    },
    "OD6S.CALLED_SHOT_MEDIUM": {
        "modifier": 12,
        "damage": 0
    },
    "OD6S.CALLED_SHOT_SMALL": {
        "modifier": 24,
        "damage": 0
    },
    "OD6S.CALLED_SHOT_HEAD": {
        "modifier": 5,
        "damage": 12,
    },
    "OD6S.CALLED_SHOT_HEART": {
        "modifier": 15,
        "damage": 12,
    },
    "OD6S.CALLED_SHOT_TORSO": {
        "modifier": 0,
        "damage": 0,
    },
    "OD6S.CALLED_SHOT_ARM": {
        "modifier": 5,
        "damage": -2,
    },
    "OD6S.CALLED_SHOT_LEG": {
        "modifier": 5,
        "damage": -1,
    },
    "OD6S.CALLED_SHOT_HAND": {
        "modifier": 15,
        "damage": -2,
    }
}

OD6S.gravity = {
    "OD6S.GRAVITY_STANDARD": {
        "modifier": 0
    },
    "OD6S.GRAVITY_LOW": {
        "modifier": -3
    },
    "OD6S.GRAVITY_NONE": {
        "modifier": -6
    },
    "OD6S.GRAVITY_HEAVY": {
        "modifier": 9
    }
}

// Other modifiers from conditions, etc.
OD6S.misc = {
    "OD6S.MISC": {
        "modifier": 0
    }
}

// attack: subtraction or addition to hit difficulty (negative numbers are in effect a bonus)
// damage: bonus or penalty to damage
// multi: whether an attack needs a ROF selection by the character for number of shots in a round
OD6S.rangedAttackOptions = {
    "OD6S.ATTACK_STANDARD": {
        "attack": 0,
        "damage": 0,
        "multi": false
    },
    "OD6S.ATTACK_RANGED_SINGLE_FIRE_AS_MULTI": {
        "attack": -3,
        "damage": +3,
        "multi": true
    },
    "OD6S.ATTACK_RANGED_FULL_AUTO": {
        "attack": -6,
        "damage": 6,
        "multi": false
    },
    "OD6S.ATTACK_RANGED_SWEEP": {
        "attack": -6,
        "damage": -9,
        "multu": false
    },
    "OD6S.ATTACK_RANGED_BURST_FIRE_AS_SINGLE": {
        "attack": 0,
        "damage": -6,
        "multi": false
    }
}

OD6S.meleeAttackOptions = {
    "OD6S.ATTACK_STANDARD": {
        "attack": 0,
        "damage": 0,
        "multi": false
    },
    "OD6S.ATTACK_ALL_OUT": {
        "attack": -6,
        "damage": 3
    },
    "OD6S.ATTACK_LUNGE": {
        "attack": 3,
        "damage": -3
    },
    "OD6S.ATTACK_KNOCKDOWN_TRIP": {
        "attack": 6,
        "damage": 0
    },
    "OD6S.ATTACK_PUSH": {
        "attack": 3,
        "damage": 0
    }
}

OD6S.brawlAttackOptions = {
    "OD6S.ATTACK_STANDARD": {
        "attack": 0,
        "damage": 0,
        "multi": false
    },
    "OD6S.ATTACK_ALL_OUT": {
        "attack": -6,
        "damage": 3
    },
    "OD6S.ATTACK_GRAB": {
        "attack": 9,
        "damage": 0
    },
    "OD6S.ATTACK_LUNGE": {
        "attack": 3,
        "damage": -3
    },
    "OD6S.ATTACK_KNOCKDOWN_TRIP": {
        "attack": 6,
        "damage": 0
    },
    "OD6S.ATTACK_PUSH": {
        "attack": 3,
        "damage": 0
    },
    "OD6S.ATTACK_SWEEP": {
        "attack": -6,
        "damage": -9
    },
    "OD6S.ATTACK_TACKLE": {
        "attack": 3,
        "damage": 0
    }
}

OD6S.attributes = {
    "agi": {
        "name": '',
        "shortName": ''
    },
    "str": {
        "name": '',
        "shortName": ''
    },
    "mec": {
        "name": '',
        "shortName": ''
    },
    "kno": {
        "name": '',
        "shortName": ''
    },
    "per": {
        "name": '',
        "shortName": ''
    },
    "tec": {
        "name": '',
        "shortName": ''
    },
    "met": {
        "name": '',
        "shortName": ''
    }
}

OD6S.ranges = {
    "OD6S.RANGE_POINT_BLANK_SHORT": {
        "name": "OD6S.RANGE_POINT_BLANK",
        "difficulty": -5,
        "map": "OD6S.DIFFICULTY_VERY_EASY"
    },
    "OD6S.RANGE_SHORT_SHORT": {
        "name": "OD6S.RANGE_SHORT",
        "difficulty": 0,
        "map": "OD6S.DIFFICULTY_EASY"
    },
    "OD6S.RANGE_MEDIUM_SHORT": {
        "name": "OD6S.RANGE_MEDIUM",
        "difficulty": 5,
        "map": "OD6S.DIFFICULTY_MODERATE"
    },
    "OD6S.RANGE_LONG_SHORT": {
        "name": "OD6S.RANGE_LONG",
        "difficulty": 10,
        "map": "OD6S.DIFFICULTY_DIFFICULT"
    }
}

OD6S.damageTypes = {
    "p": "OD6S.PHYSICAL",
    "e": "OD6S.ENERGY"
}

OD6S.cyberneticsLocations = [
    "OD6S.HEAD",
    "OD6S.RIGHT_ARM",
    "OD6S.LEFT_ARM",
    "OD6S.BODY",
    "OD6S.RIGHT_LEG",
    "OD6S.LEFT_LEG"
]

OD6S.allowedItemTypes = {
    "container": [
        "armor",
        "weapon",
        "gear",
        "cybernetic",
        "vehicle-weapon",
        "vehicle-gear",
        "starship-weapon",
        "starship-gear"
    ],
    "character": [
        "skill",
        "specialization",
        "advantage",
        "disadvantage",
        "specialability",
        "armor",
        "weapon",
        "gear",
        "cybernetic",
        "manifestation",
        "character-template",
        "species-template",
    ],
    "npc": [
        "skill",
        "specialization",
        "advantage",
        "disadvantage",
        "specialability",
        "armor",
        "weapon",
        "gear",
        "cybernetic",
        "species-template",
    ],
    "creature": [
        "skill",
        "specialization",
        "advantage",
        "disadvantage",
        "specialability",
        "armor",
        "weapon",
        "gear",
        "cybernetic"
    ],
    "vehicle": [
        "vehicle-weapon",
        "vehicle-gear"
    ],
    "starship": [
        "starship-weapon",
        "starship-gear"
    ]
}

OD6S.actorTypeLabels = {
    "character": "ACTOR.TypeCharacter",
    "creature": "ACTOR.TypeCreature",
    "npc": "ACTOR.TypeNpc",
    "starship": "ACTOR.TypeStarship",
    "vehicle": "ACTOR.TypeVehicle"
}

OD6S.templateItemTypes = {
    "character-template": [
        "skill",
        "specialability",
        "armor",
        "weapon",
        "gear",
        "cybernetic",
        "manifestation"
    ],
    "species-template": [
        "specialability"
    ],
    "item-group": [
        "skill",
        "specialability",
        "armor",
        "weapon",
        "gear",
        "cybernetic",
        "manifestation",
        "vehicle-weapon",
        "vehicle-gear",
        "starship-weapon",
        "starship-gear"
    ]
}

OD6S.itemLabels = {
    "skill": "OD6S.SKILL",
    "specialization": "OD6S.SPECIALIZATION",
    "advantage": "OD6S.ADVANTAGE",
    "disadvantage": "OD6S.DISADVANTAGE",
    "specialability": "OD6S.SPECIAL_ABILITY",
    "armor": "OD6S.ARMOR",
    "weapon": "OD6S.WEAPON",
    "gear": "OD6S.GEAR",
    "cybernetic": "OD6S.CYBERNETICS",
    "vehicle": "OD6S.VEHICLE",
    "manifestation": "OD6S.MANIFESTATION",
    "character-template": "OD6S.CHARACTER_TEMPLATE",
    "action": "OD6S.ACTION",
    "species-template": "ITEM.TypeSpecies-template",
    "starship-gear": "ITEM.TypeStarship-gear",
    "starship-weapon": "ITEM.TypeStarship-weapon",
    "vehicle-gear": "ITEM.TypeVehicle-gear",
    "vehicle-weapon": "ITEM.TypeVehicle-weapon"
}

OD6S.chatTemplates = {
    "generic": OD6S.chatPath + "generic.html",
    "roll": OD6S.chatPath + "roll.html",
    "opposed": OD6S.chatPath + "opposed.html",
    "damageresult": OD6S.chatPath + "damageresult.html"
}

OD6S.data_tab = {
    "defense": {
        "dodge": "OD6S.DODGE",
        "parry": "OD6S.PARRY",
        "block": "OD6S.BLOCK"
    },
    "offense": {
        "ranged": "OD6S.RANGED",
        "melee": "OD6S.MELEE",
        "brawl": "OD6S.BRAWL"
    }
}

OD6S.statusEffects = [
    {
        id: "dead",
        label: "EFFECT.StatusDead",
        icon: "icons/svg/skull.svg"
    },
    {
        id: "unconscious",
        label: "EFFECT.StatusUnconscious",
        icon: "icons/svg/unconscious.svg"
    },
    {
        id: "sleep",
        label: "EFFECT.StatusAsleep",
        icon: "icons/svg/sleep.svg"
    },
    {
        id: "stun",
        label: "EFFECT.StatusStunned",
        icon: "icons/svg/daze.svg"
    },
    {
        id: "prone",
        label: "EFFECT.StatusProne",
        icon: "icons/svg/falling.svg"
    },
    {
        id: "restrain",
        label: "EFFECT.StatusRestrained",
        icon: "icons/svg/net.svg",
    },
    {
        id: "paralysis",
        label: "EFFECT.StatusParalysis",
        icon: "icons/svg/paralysis.svg",
    },
    {
        id: "fly",
        label: "EFFECT.StatusFlying",
        icon: "icons/svg/wing.svg",
    },
    {
        id: "blind",
        label: "EFFECT.StatusBlind",
        icon: "icons/svg/blind.svg"
    },
    {
        id: "deaf",
        label: "EFFECT.StatusDeaf",
        icon: "icons/svg/deaf.svg"
    },
    {
        id: "silence",
        label: "EFFECT.StatusSilenced",
        icon: "icons/svg/silenced.svg"
    },
    {
        id: "fear",
        label: "EFFECT.StatusFear",
        icon: "icons/svg/terror.svg"
    },
    {
        id: "burning",
        label: "EFFECT.StatusBurning",
        icon: "icons/svg/fire.svg"
    },
    {
        id: "frozen",
        label: "EFFECT.StatusFrozen",
        icon: "icons/svg/frozen.svg"
    },
    {
        id: "shock",
        label: "EFFECT.StatusShocked",
        icon: "icons/svg/lightning.svg"
    },
    {
        id: "bleeding",
        label: "EFFECT.StatusBleeding",
        icon: "icons/svg/blood.svg"
    },
    {
        id: "disease",
        label: "EFFECT.StatusDisease",
        icon: "icons/svg/biohazard.svg"
    },
    {
        id: "poison",
        label: "EFFECT.StatusPoison",
        icon: "icons/svg/poison.svg"
    },
    {
        id: "radiation",
        label: "EFFECT.StatusRadiation",
        icon: "icons/svg/radiation.svg"
    },
    {
        id: "upgrade",
        label: "EFFECT.StatusUpgrade",
        icon: "icons/svg/upgrade.svg"
    },
    {
        id: "downgrade",
        label: "EFFECT.StatusDowngrade",
        icon: "icons/svg/downgrade.svg"
    },
    {
        id: "target",
        label: "EFFECT.StatusTarget",
        icon: "icons/svg/target.svg"
    }
]

OD6S.hitLocations = {
    0: "OD6S.LOCATION_RIGHT_HAND",
    1: "OD6S.LOCATION_LEFT_HAND",
    2: "OD6S.LOCATION_RIGHT_LEG",
    3: "OD6S.LOCATION_RIGHT_FOOT",
    4: "OD6S.LOCATION_LEFT_LEG",
    5: "OD6S.LOCATION_LEFT_FOOT",
    6: "OD6S.LOCATION_ABDOMEN",
    7: "OD6S.LOCATION_CHEST",
    8: "OD6S.LOCATION_CHEST",
    9: "OD6S.LOCATION_HEAD"
}

export default OD6S;
