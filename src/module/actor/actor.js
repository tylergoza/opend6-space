import {od6sutilities} from "../system/utilities.js";
import {od6sroll} from "../apps/od6sroll.js";
import OD6S from "../config/config-od6s.js";

/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class OD6SActor extends Actor {

    get visible(){
        if (this.type === "container" && !game.user.isGM){
            return this.data.data.visible;
        }
        else {
            return super.visible;
        }
    }

    /**
     * Augment the basic actor data with additional dynamic data.
     */
    async _preCreate(data, options, user) {
        await super._preCreate(data, options, user);

        if (this.type === 'character') {
            this.data.token.update({vision: true, actorLink: true, disposition: 1});
        }

        if (this.type === 'container') {
            this.data.token.update({vision: false, actorLink: true, disposition: 0});
            const update = {};
            update[`permission.default`] = CONST.DOCUMENT_PERMISSION_LEVELS.OWNER;
            await this.data.update(update);
        }
    }

    async _onCreate(data, options, user) {
        await super._onCreate(data, options, user);
    }

    /** @override */
    prepareData() {
        // Prepare data for the actor. Calling the super version of this executes
        // the following, in order: data reset (to clear active effects),
        // prepareBaseData(), prepareEmbeddedDocuments() (including active effects),
        // prepareDerivedData().
        super.prepareData();
    }

    /** @override */
    prepareBaseData() {
        // Data modifications in this step occur before processing embedded
        // documents or derived data.
    }

    async prepareDerivedData() {
        let actorData = this.data;

        if (this.type !== 'vehicle' && this.type !== 'starship' && this.type !== 'container') {

            if (OD6S.woundConfig === 1) {
                actorData.data.wounds.value =
                    Object.keys(Object.fromEntries(Object.entries(OD6S.deadliness[3]).filter(([k, v]) => v.description === this.getWoundLevelFromBodyPoints())))[0];
            } else if (OD6S.woundConfig === 2) {
                actorData.data.wounds.value = 0;
            }

            for (let a in actorData.data.attributes) {
                actorData.data.attributes[a].score = actorData.data.attributes[a].base + actorData.data.attributes[a].mod;
            }

            this.setStrengthDamageBonus(actorData);
            this.setInitiative(actorData);
            actorData.data.pr.score = this.setResistance('pr')
            actorData.data.er.score = this.setResistance('er')
        }
    }

    /**
     * Calculate the strength damage score.  This is one-half the dice of either lifting or strength
     *
     * @param {Object} actorData The actor to get the score of
     *
     * @return {undefined}
     */
    setStrengthDamageBonus(actorData) {
        if (actorData.type === 'vehicle' || actorData.type === 'starship' || actorData.type === 'container') return;
        if (game.settings.get('od6s', 'strength_damage')) {
            actorData.data.strengthdamage.score = actorData.data.attributes.str.score
                + actorData.data.strengthdamage.mod;
        } else {
            // See if the actor has the "lift" skill
            const lift = actorData.items.find(skill => skill.name === "Lift");
            if (lift != null && typeof (life) !== 'undefined') {
                actorData.data.strengthdamage.score =
                    Math.ceil((Math.floor((lift.data.data.score + actorData.data.attributes.str.score) / OD6S.pipsPerDice)) / 2)
                    * OD6S.pipsPerDice + actorData.data.strengthdamage.mod;
            } else {
                actorData.data.strengthdamage.score =
                    Math.ceil(Math.floor(actorData.data.attributes.str.score / OD6S.pipsPerDice) / 2) * OD6S.pipsPerDice +
                    actorData.data.strengthdamage.mod;
            }
        }
    }

    /**
     *
     * Set initiative for an actor
     *
     * @param actorData
     *
     */
    setInitiative(actorData) {
        if (actorData.type === 'vehicle' || actorData.type === 'starship' || actorData.type === 'container') return;
        let formula;
        // Base init is the character's perception score.  Special abilities and optional rules may add to it.
        let score = actorData.data.attributes.per.score + actorData.data.initiative.mod;
        let dice = od6sutilities.getDiceFromScore(score);
        let tiebreaker = actorData.data.attributes.per.score / 100 + actorData.data.attributes.agi.score / 100;
        dice.dice--;
        formula = dice.dice + "d6[Base]" + "+" + dice.pips + "+1d6x6[Wild]+" + tiebreaker;
        actorData.data.initiative.formula = formula;
        actorData.data.initiative.score = score;
        return actorData;
    }

    async applyActiveEffects() {
        const overrides = {};

        // Organize non-disabled effects by their application priority
        const changes = this.effects.reduce((changes, e) => {
            if (e.data.disabled) return changes;
            return changes.concat(e.data.changes.map(c => {
                c = foundry.utils.duplicate(c);
                c.effect = e;
                c.priority = c.priority ?? (c.mode * 10);
                return c;
            }));
        }, []);
        changes.sort((a, b) => a.priority - b.priority);
        // Apply all changes
        for (let change of changes) {
            // Check if it is an item change
            if (change.key.startsWith("data.items")) {
                const t = change.key.split('.');
                if (t[2] === 'skills' || t[2] === 'skill' || t[2] === 'specializations' || t[2] === 'specialization') {
                    const item = this.items.find(i => i.name === t[3]);
                    if (typeof (item) !== 'undefined') {
                        const update = {};
                        update.id = item.id;
                        update.data = {};
                        update.data[t[4]] = {};
                        if (change.mode === 2) {
                            update.data[t[4]] = (+change.value);
                            await item.update(update);
                        }
                        if (change.mode === 5) {
                            update.data[t[4]] = change.value;
                            await item.update(update);
                        }
                    }
                } else if (t[2] === "weapon" || t[2] === "weapons") {
                    const item = this.items.find(i => i.name === t[3]);
                    if (typeof (item) !== 'undefined') {
                        const item = this.items.find(i => i.name === t[3]);
                        const update = {};
                        update.id = item.id;
                        update.data = {};
                        if (t[4] === 'mods') {
                            update.data[t[4]] = {};
                            if (change.mode === 2) {
                                update.data[t[4]][t[5]] = (+change.value);
                                await item.update(update);
                            }
                        } else {
                            if (change.mode === 2) {
                                update.data[t[4]] = (+change.value);
                                item.update(update);
                            }
                        }
                    }
                }
            } else {
                const result = change.effect.apply(this, change);
                if (result !== null) overrides[change.key] = result;
            }
        }

        // Expand the set of final overrides
        this.overrides = foundry.utils.expandObject(overrides);

        // Calculate derived scores
        if (this.type !== 'starship' && this.type !== 'vehicle' && this.type !== 'container') {
            let actorData = this.data;
            for (let a in actorData.data.attributes) {
                actorData.data.attributes[a].score = actorData.data.attributes[a].base + actorData.data.attributes[a].mod;
            }
            this.setStrengthDamageBonus(actorData);
            this.setInitiative(actorData);
            actorData.data.pr.score = this.setResistance('pr')
            actorData.data.er.score = this.setResistance('er')
        }
    }

    async rollAttribute(attribute) {
        const data = {
            "actor": this,
            "itemId": "",
            "name": OD6S.attributes[attribute].name,
            "score": this.data.data.attributes[attribute].score,
            "type": "attribute"
        }
        await od6sroll._onRollDialog(data);
    }

    async rollAction(actionId) {
        let actor = this;
        let itemId = '';
        let name = '';
        let score = 0;
        let type = '';

        switch (actionId) {
            case "rangedattack":
            case "meleeattack":
            case "brawlattack":
            case "dodge":
            case "parry":
            case "block":
                type = actionId;
                for (let k in OD6S.actions) {
                    if (OD6S.actions[k].rollable && OD6S.actions[k].type === type) {
                        name = game.i18n.localize(OD6S.actions[k].name);
                        if (OD6S.actions[k].skill) {
                            const skill = actor.items.find(i => i.name === name);
                            if (skill !== null && typeof (skill) !== 'undefined') {
                                score = (+skill.data.data.score) +
                                    (+this.data.data.attributes[skill.data.data.attribute.toLowerCase()].score);
                            } else {
                                score = actor.data.data.attributes[OD6S.actions[k].base].score;
                            }
                        } else {
                            score = actor.data.data.attributes[OD6S.actions[k].base].score;
                        }
                    }
                }
                break;
            case 'vehiclerangedattack':
                // We know nothing about skills or fire control, just use the defaults
                type = actionId;
                name = game.i18n.localize('OD6S.ACTION_VEHICLE_RANGED_ATTACK');
                score = od6sutilities.getScoreFromSkill(actor, '', game.i18n.localize('OD6S.GUNNERY_SKILL'), 'mec');
                break;
            case 'vehicleramattack':
            case 'vehicledodge':
            case 'vehiclemaneuver':
                type = actionId;
                for (let k in OD6S.vehicle_actions) {
                    if (OD6S.vehicle_actions[k].rollable && OD6S.vehicle_actions[k].type === type) {
                        type = actionId;
                        name = game.i18n.localize(OD6S.vehicle_actions[k].name);
                        score = od6sutilities.getScoreFromSkill(
                            actor,
                            actor.data.data.vehicle.specialization.value,
                            actor.data.data.vehicle.skill.value,
                            OD6S.vehicle_actions[k].base) + actor.data.data.vehicle.maneuverability.score;
                    }
                }
                break;
            case 'vehicletoughness':
                type = "vehicletoughness";
                score = this.data.data.vehicle.toughness.score;
                if (this.data.data.vehicle.type === 'vehicle') {
                    name = game.i18n.localize(OD6S.vehicleToughnessName);
                } else {
                    name = game.i18n.localize(OD6S.starshipToughnessName);
                }
                break;
            case 'vehicleshieldsfront':
                type = "vehicletoughness";
                score = this.data.data.vehicle.shields.arcs.front.value + this.data.data.vehicle.toughness.score;
                name = game.i18n.localize(actor.data.data.vehicle.shields.arcs.front.label) + " " +
                    game.i18n.localize('OD6S.SHIELDS');
                break;
            case 'vehicleshieldsrear':
                type = "vehicletoughness";
                score = this.data.data.vehicle.shields.arcs.rear.value + this.data.data.vehicle.toughness.score;
                name = game.i18n.localize(actor.data.data.vehicle.shields.arcs.rear.label) + " " +
                    game.i18n.localize('OD6S.SHIELDS');
                break;
            case 'vehicleshieldsleft':
                type = "vehicletoughness";
                score = this.data.data.vehicle.shields.arcs.left.value + this.data.data.vehicle.toughness.score;
                name = game.i18n.localize(actor.data.data.vehicle.shields.arcs.left.label) + " " +
                    game.i18n.localize('OD6S.SHIELDS');
                break;
            case 'vehicleshieldsright':
                type = "vehicletoughness";
                score = this.data.data.vehicle.shields.arcs.right.value + this.data.data.vehicle.toughness.score;
                name = game.i18n.localize(actor.data.data.vehicle.shields.arcs.right.label) + " " +
                    game.i18n.localize('OD6S.SHIELDS');
                break;
            case 'vehiclesensorspassive':
            case 'vehiclesensorsfocus':
            case 'vehiclesensorsscan':
            case 'vehiclesensorssearch':
                const sensorType = actionId.replace('vehiclesensors', '');
                score = od6sutilities.getSensorTotal(actor, actor.data.data.vehicle.sensors.types[sensorType].score);
                name = game.i18n.localize('OD6S.SENSORS') + ": " +
                    game.i18n.localize(actor.data.data.vehicle.sensors.types[sensorType].label);
                break;
            case "er":
                name = game.i18n.localize(actor.data.data.er.label)
                score = actor.data.data.er.score;
                break;

            case "pr":
                name = game.i18n.localize(actor.data.data.pr.label)
                score = actor.data.data.pr.score;
                break;

            default:
                let item = actor.items.find(i => i.id === actionId);
                if (item !== null && typeof (item) !== 'undefined') {
                    return await item.roll()
                } else {
                    type = 'vehiclerangedweaponattack';
                    item = actor.data.data.vehicle.vehicle_weapons.filter(i => i.id === actionId)[0];
                    if (item !== null && typeof (item) !== 'undefined') {
                        name = item.name;
                        itemId = item._id;
                        // Add spec/skill/attribute/fire control
                        score = od6sutilities.getScoreFromSkill(
                            actor,
                            item.data.specialization.value,
                            game.i18n.localize(item.data.skill.value),
                            item.data.attribute.value) + (+item.data.fire_control.score);
                    }
                }
        }

        const data = {
            "actor": this,
            "itemId": itemId,
            "name": name,
            "score": score,
            "type": "action",
            "subtype": type
        }

        await od6sroll._onRollDialog(data);
    }

    async applyDamage(damage) {
        const update = {};
        update.id = this.id;
        update[`data.damage.value`] = this.calculateNewDamageLevel(damage);
        await this.update(update);
    }

    calculateNewDamageLevel(damage) {
        if (damage === 'OD6S.DAMAGE_DESTROYED') return damage;
        const currentDamageLevel = this.data.data.damage.value;
        if (currentDamageLevel === 'OD6S.DAMAGE_NONE') {
            return (damage);
        } else if (currentDamageLevel === 'OD6S.DAMAGE_VERY_LIGHT') {
            if (damage === 'OD6S.DAMAGE_VERY_LIGHT') return damage;
            return damage;
        } else if (currentDamageLevel === 'OD6S.DAMAGE_LIGHT') {
            if (damage === 'OD6S.DAMAGE_VERY_LIGHT') return currentDamageLevel;
            if (damage === 'OD6S.DAMAGE_LIGHT') return damage;
            return damage;
        } else if (currentDamageLevel === 'OD6S.DAMAGE_HEAVY') {
            if (damage === 'OD6S.DAMAGE_VERY_LIGHT') return currentDamageLevel;
            if (damage === 'OD6S.DAMAGE_LIGHT') return 'OD6S.DAMAGE_SEVERE';
            if (damage === 'OD6S.DAMAGE_HEAVY') return 'OD6S.DAMAGE_SEVERE';
            return damage;
        } else if (currentDamageLevel === 'OD6S.DAMAGE_SEVERE') {
            if (damage === 'OD6S.DAMAGE_VERY_LIGHT') return currentDamageLevel;
            if (damage === 'OD6S.DAMAGE_LIGHT') return 'OD6S.DAMAGE_DESTROYED';
            if (damage === 'OD6S.DAMAGE_HEAVY') return 'OD6S.DAMAGE_DESTROYED';
            if (damage === 'OD6S.DAMAGE_SEVERE') return 'OD6S.DAMAGE_DESTROYED';
        }
    }

    async applyWounds(wound) {
        const update = {};
        update.id = this.id;
        update[`data.wounds.value`] = this.calculateNewWoundLevel(wound);
        await this.update(update);
    }

    findFirstWoundLevel(table, wound) {
        for (const level in table) {
            if (table[level].core === wound) return level;
        }
    }

    calculateNewWoundLevel(wound) {
        const deadlinessTable = OD6S.deadliness[OD6S.deadlinessLevel[this.type]];
        const currentWoundLevel = this.data.data.wounds.value;
        const currentWoundCore = deadlinessTable[currentWoundLevel].core;
        if (wound === 'OD6S.WOUNDS_DEAD') return this.findFirstWoundLevel(deadlinessTable, wound);
        // If the wound table has no "stunned" result promote it to a Wound
        if (wound === 'OD6S.WOUNDS_STUNNED' && !this.findFirstWoundLevel(deadlinessTable, wound)) wound = 'OD6S.WOUNDS_WOUNDED';
        // No incapacitated, promote it to Mortally Wounded
        if (wound === 'OD6S.WOUNDS_INCAPACITATED' && !this.findFirstWoundLevel(deadlinessTable, wound)) wound = 'OD6S.WOUNDS_MORTALLY_WOUNDED';

        if (currentWoundCore === 'OD6S.WOUNDS_HEALTHY') {
            return this.findFirstWoundLevel(deadlinessTable, wound);
        } else if (currentWoundCore === 'OD6S.WOUNDS_STUNNED') {
            return this.findFirstWoundLevel(deadlinessTable, wound);
        } else if (currentWoundCore === 'OD6S.WOUNDS_WOUNDED') {
            if (wound === 'OD6S.WOUNDS_STUNNED') return currentWoundLevel;
            if (wound === 'OD6S.WOUNDS_WOUNDED') return (+currentWoundLevel) + 1;
            return this.findFirstWoundLevel(deadlinessTable, wound);
        } else if (currentWoundCore === 'OD6S.WOUNDS_SEVERELY_WOUNDED') {
            if (wound === 'OD6S.WOUNDS_STUNNED') return currentWoundLevel;
            if (wound === 'OD6S.WOUNDS_WOUNDED') return (+currentWoundLevel) + 1;
            return this.findFirstWoundLevel(deadlinessTable, wound);
        } else if (currentWoundCore === 'OD6S.WOUNDS_INCAPACITATED') {
            if (wound === 'OD6S.WOUNDS_STUNNED') return currentWoundLevel;
            if (wound === 'OD6S.WOUNDS_WOUNDED') return (+currentWoundLevel) + 1;
            if (wound === 'OD6S.WOUNDS_INCAPACITATED') return (+currentWoundLevel) + 1;
            return this.findFirstWoundLevel(deadlinessTable, wound);
        } else if (currentWoundCore === 'OD6S.WOUNDS_MORTALLY_WOUNDED') {
            if (wound === 'OD6S.WOUNDS_STUNNED') return currentWoundLevel;
            if (wound === 'OD6S.WOUNDS_WOUNDED') return currentWoundLevel;
            if (wound === 'OD6S.WOUNDS_INCAPACITATED') return (+currentWoundLevel) + 1;
            if (wound === 'OD6S.WOUNDS_MORTALLY_WOUNDED') return (+currentWoundLevel + 1);
            return this.findFirstWoundLevel(deadlinessTable, wound);
        }
    }


    getWoundLevelFromBodyPoints(bp) {
        if (this.type === 'vehicle' || this.type === 'starship') return;
        let bodyPointsCurrent;
        if (typeof (bp) !== 'undefined') {
            bodyPointsCurrent = bp;
        } else {
            bodyPointsCurrent = this.data.data.wounds.body_points.current
        }

        if (bodyPointsCurrent < 1) return 'OD6S.WOUNDS_DEAD';
        const ratio = Math.ceil(bodyPointsCurrent / this.data.data.wounds.body_points.max * 100)
        let level;
        for (const key in OD6S.bodyPointLevels) {
            if (ratio < OD6S.bodyPointLevels[key]) {
                level = key;
            } else {
                break;
            }
        }
        if (typeof (level) === 'undefined') level = 'OD6S.WOUNDS_HEALTHY';
        return level;
    }

    async setWoundLevelFromBodyPoints(bp) {
        const update = {};
        update[`data.wounds.body_points.current`] = bp;
        await this.update(update);
        update['data.wounds.value'] =
            Object.keys(Object.fromEntries(Object.entries(OD6S.deadliness[3]).filter(([k, v]) => v.description === this.getWoundLevelFromBodyPoints())))[0];
        await this.update(update);
    }


    setResistance(type) {
        // Get PR from armor and add it to strength
        if (this.type === 'vehicle' || this.type === 'starship' || this.type === 'container') return 0;
        let dr = this.data.data.attributes.str.score + this.data.data[type].mod;
        if (this.itemTypes.armor) {
            this.itemTypes.armor.forEach((value, index, array) => {
                if (value.data.data.equipped.value)
                    dr += value.data.data[type];
            })
        }
        return dr;
    }

    /**
     * Flags the actor as a member of a vehicle crew
     * @param vehicleID
     */
    async addToCrew(vehicleID) {
        if (this.isCrewMember()) {
            ui.notifications.warn(game.i18n.localize('OD6S.ALREADY_CREW'));
            return false;
        } else {
            return await this.setFlag('od6s', 'crew', vehicleID);
        }
    }

    /**
     * Remove an actor as a vehicle crew member
     * @param vehicleID
     */
    async removeFromCrew(vehicleID) {
        if (this.getFlag('od6s', 'crew') !== vehicleID) {
            ui.notifications.warn(game.i18n.localize('OD6S.NOT_CREW_MEMBER'))
        } else {
            await this.setFlag('od6s', 'crew', '');
        }
    }

    /**
     * Check crew member flag
     * @returns {boolean}
     */
    isCrewMember() {
        return !(this.getFlag('od6s', 'crew') === ''
            && typeof (this.getFlag('od6s', 'crew')) !== "undefined");
    }

    async useCharacterPointOnRoll(message) {
        // Roll 1d6x6 and deduct a character point from the actor
        const actor = game.actors.get(message.data.speaker.actor);
        const rollString = "1d6x6[CP]";
        let roll = await new Roll(rollString).evaluate({"async": true});
        if (game.modules.get('dice-so-nice') && game.modules.get('dice-so-nice').active) {
            game.dice3d.showForRoll(roll, game.user, true, false, false);
        }

        const update = {};
        update.id = actor.id;
        update.data = {};
        update.data.characterpoints = {};
        update.data.characterpoints.value = actor.data.data.characterpoints.value -= 1;

        switch (message.getFlag('od6s', 'subtype')) {
            case "dodge":
                update.data.dodge = {};
                update.data.dodge.score = actor.data.data.dodge.score + roll.total;
                break;
            case "parry":
                update.data.parry = {};
                update.data.parry.score = actor.data.data.parry.score + roll.total;
                break;
            case "block":
                update.data.block = {};
                update.data.block.score = actor.data.data.block.score + roll.total;
                break;
            default:
                break;
        }

        await actor.update(update);

        // Update original card and re-display
        let replacementRoll = JSON.parse(JSON.stringify(message.roll));
        replacementRoll.dice.push(roll.dice[0]);
        replacementRoll.total += roll.total;

        const messageUpdate = {};
        messageUpdate.data = {};
        messageUpdate.content = replacementRoll.total;
        messageUpdate.id = message.id;
        messageUpdate.roll = replacementRoll;

        if (game.user.isGM) {
            await message.update(messageUpdate, {"diff": true});
            await message.setFlag('od6s', 'total', replacementRoll.total);
            if ((+messageUpdate.content) >= (message.getFlag('od6s', 'difficulty'))) {
                await message.setFlag('od6s', 'success', true);
            }
        } else {
            game.socket.emit('system.od6s', {
                operation: 'updateRollMessage',
                message: message,
                update: messageUpdate
            })
        }

        // Is this an init roll?
        if (message.getFlag('core', 'initiativeRoll')) {
            if (game.user.isGM) {
                if (game.combat !== null) {
                    const combatant = game.combat.data.combatants.find(c => c.actor.id === actor.id);
                    const update = {
                        id: combatant.id,
                        initiative: replacementRoll.total
                    }
                    await combatant.update(update);
                }
            } else {
                game.socket.emit('system.od6s', {
                    operation: "updateInitRoll",
                    message: message,
                    update: messageUpdate
                })
            }
        }
    }

    async modifyShields(update) {
        await OD6S.socket.executeAsGM("modifyShields", update);
    }

    /**
     * Send vehicle data to GM to populate crew vehicle data
     * @returns {Promise<void>}
     */
    async sendVehicleData() {
        const data = {};
        data.uuid = this.uuid;
        data.name = this.name;
        data.type = this.type;
        data.move = this.data.data.move;
        data.maneuverability = this.data.data.maneuverability;
        data.toughness = this.data.data.toughness;
        data.crewmembers = this.data.data.crewmembers;
        data.items = this.items;
        data.attribute = this.data.data.attribute;
        data.skill = this.data.data.skill;
        data.specialization = this.data.data.specialization;
        data.damage = this.data.data.damage;
        data.shields = this.data.data.shields;
        data.scale = this.data.data.scale;
        data.sensors = this.data.data.sensors;
        data.armor = this.data.data.armor;
        data.dodge = this.data.data.dodge;
        data.ranged = this.data.data.ranged;
        data.ranged_damage = this.data.data.ranged_damage;
        data.ram = this.data.data.ram;
        data.ram_damage = this.data.data.ram_damage;
        data.vehicle_weapons = [];
        for (let i = 0; i < data.items.size; i++) {
            if (this.data.items.contents[i].type === "vehicle-weapon" || this.data.items.contents[i].type === "starship-weapon") {
                let newItem = this.data.items.contents[i].toObject()
                newItem.id = this.data.items.contents[i].id;
                data.vehicle_weapons.push(newItem);
            }
        }

        if (game.user.isGM) {
            for (const e of data.crewmembers) {
                let actor = await od6sutilities.getActorFromUuid(e.uuid);
                if (actor) {
                    const update = {};
                    update.id = actor.id;
                    update.data = {}
                    update.data.vehicle = data;
                    await actor.update(update);
                }
            }
        } else {
            await OD6S.socket.executeAsGM("sendVehicleData", data);
        }
    }

    /**
     * Roll a generic collision for a vehicle.
     * @returns {Promise<void>}
     */
    async vehicleCollision() {
        if (this.type !== 'vehicle' && this.type !== 'starship') {
            ui.notifications.warn(game.i18n.localize('OD6S.WARN_ACTOR_NOT_VEHICLE'));
            return;
        }
        const html = await renderTemplate("systems/od6s/templates/actor/vehicle/collision.html");
        new Dialog({
            title: game.i18n.localize('OD6S.ROLL_COLLISION_DAMAGE'),
            content: html,
            buttons: {
                roll: {
                    label: game.i18n.localize('OD6S.ROLL'),
                    callback: async (dlg) => {
                        const speed = $(dlg[0]).find("#vehiclespeed")[0].value;
                        const speedValue = OD6S.vehicle_speeds[speed].damage;
                        const type = $(dlg[0]).find("#vehiclecollisiontype")[0].value;
                        const typeValue = OD6S.collision_types[type].score;
                        const mod = $(dlg[0]).find("#vehiclecollisionmod")[0].value;
                        const score = (+speedValue) + (+typeValue) + (+mod * OD6S.pipsPerDice);
                        const dice = od6sutilities.getDiceFromScore(score);
                        let rollString;
                        if (game.settings.get('od6s', 'use_wild_die')) {
                            dice.dice = dice.dice - 1;
                            if (dice.dice < 1) {
                                rollString = "+1dw" + game.i18n.localize("OD6S.WILD_DIE_FLAVOR");
                            } else {
                                rollString = dice.dice + "d6" + game.i18n.localize('OD6S.BASE_DIE_FLAVOR') + "+1dw" +
                                    game.i18n.localize("OD6S.WILD_DIE_FLAVOR");
                            }
                        } else {
                            rollString = dice.dice + "d6" + +game.i18n.localize('OD6S.BASE_DIE_FLAVOR');
                        }
                        dice.pips ? rollString += "+" + dice.pips : null;
                        let roll = await new Roll(rollString).evaluate({"async": true});
                        let label = game.i18n.localize('OD6S.DAMAGE') + " (" +
                            game.i18n.localize(OD6S.damageTypes['p']) + ") "
                            + game.i18n.localize("OD6S.FROM") + " " + game.i18n.localize("OD6S.COLLISION");

                        const flags = {
                            "type": "damage",
                            "source": game.i18n.localize("OD6S.COLLISION"),
                            "damageType": "p",
                            "targetName": null,
                            "targetId": null,
                            "isOpposable": true,
                            "wild": false,
                            "wildHandled": false,
                            "wildResult": OD6S.wildDieResult[OD6S.wildDieOneDefault],
                            "total": roll.total,
                            "isVehicleCollision": true
                        }

                        if (game.settings.get('od6s', 'use_wild_die')) {
                            if (roll.terms.find(d => d.flavor === "Wild").total === 1) {
                                flags.wild = true;
                                if (OD6S.wildDieOneDefault > 0 && OD6S.wildDieOneAuto === 0) {
                                    flags.wildHandled = true;
                                }
                            } else {
                                flags.wild = false;
                            }
                        }

                        let rollMode = 'roll';
                        if (game.user.isGM && game.settings.get('od6s', 'hide-gm-rolls')) rollMode = CONST.DICE_ROLL_MODES.PRIVATE;

                        let rollMessage = await roll.toMessage({
                            speaker: ChatMessage.getSpeaker({actor: game.actors.find(a => a.id === this.id)}),
                            flavor: label,
                            flags: {
                                od6s: flags
                            },
                            rollMode: rollMode, create: true
                        });

                        if (flags.wild === true && OD6S.wildDieOneDefault === 2 && OD6S.wildDieOneAuto === 0) {
                            let replacementRoll = JSON.parse(JSON.stringify(rollMessage.roll.toJSON()));
                            let highest = 0;
                            for (let i = 0; i < replacementRoll.terms[0].results.length; i++) {
                                replacementRoll.terms[0].results[i].result >
                                replacementRoll.terms[0].results[highest].result ?
                                    highest = i : {}
                            }
                            replacementRoll.terms[0].results[highest].discarded = true;
                            replacementRoll.terms[0].results[highest].active = false;
                            replacementRoll.total -= (+replacementRoll.terms[0].results[highest].result);
                            const rollMessageUpdate = {};
                            rollMessageUpdate.data = {};
                            rollMessageUpdate.content = replacementRoll.total;
                            rollMessageUpdate.id = rollMessage.id;
                            rollMessageUpdate.roll = replacementRoll;

                            if (rollMessage.getFlag('od6s', 'difficulty') && rollMessage.getFlag('od6s', 'success')) {
                                replacementRoll.total < rollMessage.getFlag('od6s', 'difficulty') ? await rollMessage.setFlag('od6s', 'success', false) :
                                    await rollMessage.setFlag('od6s', 'success', true);
                            }

                            await rollMessage.setFlag('od6s', 'originalroll', rollMessage.roll)

                            await rollMessage.update(rollMessageUpdate, {"diff": true});
                        }
                    }
                }
            }
        }).render(true);
    }

    /**
     * Handle creating a new item for a vehicle cargo hold
     * @param event
     * @private
     */
    async onCargoHoldItemCreate(event) {
        event.preventDefault();

        const documentName = 'Item';
        let types, folders, label, title, template;
        types = game.system.documentTypes[documentName];
        let data = {};
        folders = game.folders.filter(f => (f.data.type === documentName) && f.displayed);
        label = game.i18n.localize('OD6S.ITEM');
        title = game.i18n.format("OD6S.CREATE_ITEM", {entity: label});
        template = 'templates/sidebar/document-create.html';

        if (game.settings.get('od6s', 'hide_advantages_disadvantages')) {
            types = types.filter(function (value, index, arr) {
                return value !== 'advantage';
            })
            types = types.filter(function (value, index, arr) {
                return value !== 'disadvantage';
            })
        }

        types = types.filter(t => OD6S.cargo_hold.includes(t));
        types = types.filter(t => !t.startsWith(this.type));

        types = types.sort(function (a, b) {
            return a.localeCompare(b);
        })

        // Render the entity creation form
        const html = await renderTemplate(template, {
            name: data.name || game.i18n.format("OD6S.NEW_ITEM", {entity: label}),
            type: data.type || types[0],
            types: types.reduce((obj, t) => {
                const label = CONFIG[documentName]?.typeLabels?.[t] ?? t;
                obj[t] = game.i18n.has(label) ? game.i18n.localize(label) : t;
                return obj;
            }, {}),
            hasTypes: types.length > 1
        });

        // Render the confirmation dialog window
        return Dialog.prompt({
            title: title,
            content: html,
            label: title,
            callback: html => {
                const form = html[0].querySelector("form");
                const fd = new FormDataExtended(form);
                data = foundry.utils.mergeObject(data, fd.toObject());
                if (!data.folder) delete data["folder"];
                if (types.length === 1) data.type = types[0];
                data.name = data.name || game.i18n.localize('OD6S.NEW') + " " + game.i18n.localize(OD6S.itemLabels[data.type]);
                return this.createEmbeddedDocuments('Item', [data]);
            },
            rejectClose: false
        });
    }
}
