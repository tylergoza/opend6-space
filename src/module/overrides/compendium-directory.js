export class OD6SCompendiumDirectory extends CompendiumDirectory {

    getData(options) {

        game.compendiumDirectory = this;

        // Filter packs for visibility
        let basePacks = game.packs.filter(p => game.user.isGM || !p.private);
        let packs;
        // If hidden compendia is turned on, filter out 'od6s' item compendia
        if (game.settings.get('od6s','hide_compendia')) {
            //packs = basePacks.filter(p => p.metadata.system !== 'od6s');
            packs = basePacks.filter(function(obj) {
                return !(obj.metadata.system === 'od6s' && obj.documentName !== 'Macro');
            })
        } else {
            // Hide advantages/disadvantages if set
            if (game.settings.get('od6s','hide_advantages_disadvantages')) {
              basePacks = basePacks.filter(p => p.metadata.name !== 'advantages');
              basePacks = basePacks.filter(p => p.metadata.name !== 'disadvantages');
            }
            packs = basePacks
        }

        // Sort packs by Entity type
        const packData = packs.sort((a,b) => a.documentName.localeCompare(b.documentName)).reduce((obj, pack) => {
            const documentName = pack.documentClass.documentName;
            if ( !obj.hasOwnProperty(documentName) ) obj[documentName] = {
                label: documentName,
                packs: []
            };
            obj[documentName].packs.push(pack);
            return obj;
        }, {});

        // Sort packs within type
        for ( let [e, p] of Object.entries(packData) ) {
            p.packs = p.packs.sort((a,b) => a.title.localeCompare(b.title));
        }

        // Return data to the sidebar
        return {
            user: game.user,
            packs: packData
        }
    }

}